
System.print("hello from wren")

import "math" for Vec2, Vec3, Vec4, Mat2
import "ecs" for WorldWrapper
var tvec3 = Vec3.new(1, 2, 3)
System.print(tvec3.toString())

var t = 0

import "ecs" for RelativeTransformComponent

main = Fn.new {|world|
    var entities = world.getEntities()
    /*for(entity in entities) {
        System.print("entity")
        System.print(entity)
        var components = world.getEntityComponents(entity)
        for(component in components) {
            System.print(component.toString())
            if(component.toString() == "TagComponent" && component.getField("name") == "Player entity") {
                System.print(component.getField("name"))
                System.print("////////////Found player")
            }
            var position = component.getField("position")
            if (position) {
                System.print("Position:" + component.getField("position").toString())
            }
        }
    }*/
  t = t + 0.1
  entities.each {|entity| 
    var components = world.getEntityComponents(entity)
    var isCamera = components.any{|component|
      return component.getField("name") == "Antique Camera"
    }
    if (isCamera) {
      components.each {|component|
        if(component.toString() == "RelativeTransformComponent") {
          component.position = Vec3.new(t.sin, 0.0, t.cos)*3
        }
      }
    }
  }
    //System.print(entities)
}
