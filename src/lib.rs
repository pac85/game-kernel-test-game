pub mod dbg_ui;
pub mod docking;
pub mod enemy;
pub mod game;
pub mod game_ui;
pub mod networking;
pub mod physics;
pub mod player;
pub mod scripting;
pub mod systems;
pub mod utils;

use cgmath;
use game_kernel::core_systems::{decal::DecalComponent, uuid::UuidGenrator};
use game_kernel::rodio::Source;
use game_kernel::vulkan::VulkanBasic;
use game_kernel::{self, CameraComponent, CameraSystem, TransformComponent};
use game_kernel_ecs;

use ::game_kernel::core_systems::{
    relative_transform::RelativeTransformComponent, tag::TagComponent,
};
use ::game_kernel::ecs::{AddEntity, EntitId, World};
use ::game_kernel::subsystems::video::ui::egui::GkEgui;
use ::game_kernel::subsystems::Subsystems;
use ::game_kernel::MainRenderer;
use cgmath::*;
use egui;
use game_kernel::renderer::{
    light::*, material::Material, mesh::*, model::Model, particle_fx::*, texture::*,
};
use game_kernel_editor::ui::EditorUi;
use networking::netsys::{ClientSystem, ServerSystem};
use rqmap::parser::ss::Parse;

use std::any::Any;
use std::cell::RefCell;
use std::collections::HashMap;
use std::convert::TryFrom;
use std::fs::File;
use std::io::*;
use std::sync::Arc;
use std::time::Duration;

pub struct EguiTexture {
    id: egui::TextureId,
    size: [u32; 2],
}

use game_kernel::input::{Input, MouseKeys};
use std::ops::Deref;

fn setup_input(sbsts: &mut Subsystems) {
    use game_kernel::input::KeyTypes;
    sbsts.input().bind_action(KeyTypes::KeyBoard(17), "forward");
    sbsts
        .input()
        .bind_action(KeyTypes::KeyBoard(31), "backward");
    sbsts.input().bind_control("forward", "forward", 1.0);
    sbsts.input().bind_control("backward", "forward", -1.0);

    sbsts.input().bind_action(KeyTypes::KeyBoard(32), "left");
    sbsts.input().bind_action(KeyTypes::KeyBoard(30), "right");
    sbsts.input().bind_control("left", "side", 1.0);
    sbsts.input().bind_control("right", "side", -1.0);

    sbsts.input().bind_action(KeyTypes::KeyBoard(57), "jump");

    sbsts.input().bind_action(KeyTypes::KeyBoard(2), "weapon1");
    sbsts.input().bind_action(KeyTypes::KeyBoard(3), "weapon2");
    sbsts.input().bind_action(KeyTypes::KeyBoard(4), "weapon3");
    sbsts.input().bind_action(KeyTypes::KeyBoard(5), "weapon4");
    sbsts
        .input()
        .bind_action(KeyTypes::Mouse(MouseKeys::Left), "shoot");
    sbsts.input().bind_action(KeyTypes::KeyBoard(18), "pickup");
}

fn setup_flycam(sbsts: &mut ::game_kernel::subsystems::Subsystems) -> EntitId {
    //sets up camera
    let camera = game_kernel::Camera::new(
        Point3::new(0.0, 0.0, -6.0),
        Vector3::new(1.0, 0.0, 0.0),
        Vector3::new(0.0, -1.0, 0.0),
        Rad::turn_div_4() / 1f32,
        (0.1, 1000.0),
    );

    let camera_entity = AddEntity::new(sbsts.world.borrow_mut(), None)
        .unwrap()
        .with_component(game_kernel::CameraComponent::new(camera))
        .with_component(game_kernel::TransformComponent::identity())
        .entity;

    sbsts
        .systems_manager
        .get_system_mut::<game_kernel::CameraSystem>()
        .unwrap()
        .set_active_camera(camera_entity);

    camera_entity
}

fn flycam<W: Deref<Target = World>>(
    world: &W,
    camera_entity_index: EntitId,
    delta_time: f32,
    r: Vector2<f32>,
    input: &Input,
) {
    let mut camera = world
        .get_entity_components::<&mut CameraComponent>(camera_entity_index)
        .unwrap();

    camera.camera.dir =
        cgmath::Vector3::new(r.x.sin() * r.y.cos(), -r.y.sin(), r.x.cos() * r.y.cos());

    use cgmath::prelude::InnerSpace;
    let velocity = if input.is_action_down("sprint") {
        0.04
    } else {
        0.02
    };

    let ocamera = camera.camera;
    drop(camera);

    let mut c = world
        .get_entity_components::<&mut TransformComponent>(camera_entity_index)
        .unwrap();

    c.position += ocamera.dir * delta_time * velocity * input.get_control_value("forward").unwrap();
    c.position += ocamera.dir.cross(ocamera.up).normalize()
        * delta_time
        * velocity
        * input.get_control_value(&"side".to_owned()).unwrap();

    let cr = Matrix3::look_at(ocamera.dir, ocamera.up);
    let cr = Quaternion::from(cr.invert().unwrap());

    c.rotation = cr;
}

pub enum MainArgs {
    Local,
    Server(ServerSystem),
    Client(ClientSystem),
}

pub fn generic_main(main_args: MainArgs, seed: u64) {
    dbg_ui::init_screen_console();

    systems::register_components();
    player::register();
    networking::netsys::register_components();

    let mut sbsts = ::game_kernel::subsystems::init().unwrap();

    sbsts
        .physfs()
        .mount("./files", "/", false)
        .unwrap_or_else(|_| {});

    sbsts
        .physfs()
        .mount("./ripped_files", "/", false)
        .unwrap_or_else(|_| {});

    sbsts
        .physfs()
        .mount("wad.pac", "/", false)
        .unwrap_or_else(|_| println!("failed to mount wad.pac"));

    sbsts
        .physfs()
        .mount("sponza.pac", "/", false)
        .unwrap_or_else(|_| println!("failed to mount sponza.pac"));

    ::game_kernel::core_systems::init_systems(&mut sbsts);
    systems::init_systems(&mut sbsts.systems_manager, &mut *sbsts.world.borrow_mut());

    match main_args {
        MainArgs::Local => {}
        MainArgs::Server(server_system) => {
            sbsts
                .systems_manager
                .add_and_init(server_system, &*sbsts.world.borrow())
                .unwrap();
        }
        MainArgs::Client(client_system) => {
            sbsts
                .systems_manager
                .add_and_init(client_system, &*sbsts.world.borrow())
                .unwrap();
        }
    }

    enemy::register_components();
    let vk = sbsts.get_vulkan_common().unwrap();
    enemy::init(
        &mut sbsts.systems_manager,
        &mut *sbsts.world.borrow_mut(),
        vk,
    );

    let renderer = sbsts.renderer.renderer.as_ref().unwrap().clone();

    //raytracing test
    /*{
        let renderer = sbsts.renderer.renderer.as_ref().unwrap().clone();

        let mut renderer = renderer.borrow_mut();

        let k = renderer.add_render_hook::<game_kernel::renderer::raytracing::RayTracer>();

        let (document, buffers, images) = gltf::import("files/meshes/suz.glb").unwrap();

        let mut nodes = document.default_scene().unwrap().nodes();

        let suzanne_data = MeshData::try_from((
            nodes
                .next()
                .unwrap()
                .mesh()
                .unwrap()
                .primitives()
                .next()
                .unwrap(),
            &buffers[0].clone(),
        ))
        .unwrap();

        let suzanne_mesh =
            game_kernel::subsystems::video::renderer::mesh::Mesh::StaticMesh(
                StaticMesh::from_mesh_data(sbsts.get_vulkan_common().unwrap(), &suzanne_data),
            );


        for i in suzanne_data.index_buffer.iter() {
            println!("index {}", i);
        }

        for (j, i) in suzanne_data.vertex_buffer.iter().enumerate() {
            println!("{} vertex {:?}", j, i);
        }

        println!("BVH build");
        if let game_kernel::renderer::mesh::Mesh::StaticMesh(sm) = suzanne_mesh {

            let rt = renderer
                .get_render_hook_mut(k)
                .unwrap()
                .as_any_mut()
                .downcast_mut::<game_kernel::renderer::raytracing::RayTracer>()
                .unwrap();
            rt
                .insert_mesh(&suzanne_data, &sm);
        }
        println!("BVH build end");
    }*/

    /*renderer
    .borrow_mut()
    .add_decal(game_kernel::renderer::decals::Decal::new(
        crate::utils::test_pbr_textures(&*sbsts.physfs(), sbsts.get_vulkan_common().unwrap()),
        Matrix4::from_translation(vec3(0.0, -10.0, 0.0)) * Into::<Matrix4<f32>>::into(Matrix3::from_angle_x(Deg(90.0))),
        //cgmath::ortho(-5.0, 5.0, -5.0, 5.0, -5.0, 5.0) * Matrix4::look_at_dir(cgmath::Point3::new(0.0, -10.0, 0.0), vec3(0.0, -1.0, 0.0), vec3(1.0, 0.0, 0.0))
    ));*/

    let _decal = AddEntity::new(sbsts.world.borrow_mut(), None)
        .unwrap()
        .with_component(TagComponent::new("Test decal"))
        .with_component(
            RelativeTransformComponent::identity()
                .translate(vec3(0.0, -10.0, 0.0))
                .rotate(Matrix3::from_angle_x(Deg(90.0))),
        )
        .with_component(DecalComponent::new(
            game_kernel::renderer::decals::Decal::new(
                crate::utils::test_pbr_textures(
                    &*sbsts.physfs(),
                    sbsts.get_vulkan_common().unwrap(),
                ),
                Matrix4::identity(),
                //cgmath::ortho(-5.0, 5.0, -5.0, 5.0, -5.0, 5.0) * Matrix4::look_at_dir(cgmath::Point3::new(0.0, -10.0, 0.0), vec3(0.0, -1.0, 0.0), vec3(1.0, 0.0, 0.0))
            ),
        ))
        .entity;

    let antique_camera = AddEntity::new(sbsts.world.borrow_mut(), None)
        .unwrap()
        .with_component(TagComponent::new("Antique Camera"))
        //.with_component(utils::load_test_cube(sbsts.get_vulkan_common().unwrap()))
        .with_component(RelativeTransformComponent::identity())
        .entity;

    utils::load_gltf_into_world(
        "AntiqueCamera.glb",
        &mut *sbsts.world.borrow_mut(),
        antique_camera,
        sbsts.get_vulkan_common().unwrap(),
        &*sbsts.physfs(),
    );

    utils::load_gltf_into_world(
        "grid.glb",
        &mut *sbsts.world.borrow_mut(),
        World::get_root(),
        sbsts.get_vulkan_common().unwrap(),
        &*sbsts.physfs(),
    );

    utils::load_gltf_into_world(
        "tube.glb",
        &mut *sbsts.world.borrow_mut(),
        World::get_root(),
        sbsts.get_vulkan_common().unwrap(),
        &*sbsts.physfs(),
    );

    /*utils::quake_map::quke_map_to_scene_parent(
        &rqmap::MapAndWad::new(&mut BufReader::new(
            File::open("/home/antonino/cose/trenchbroomtest/E1M2-m.MAP").unwrap(),
        ))
        .unwrap(),
        sbsts.get_vulkan_common().unwrap(),
        &mut *sbsts.world.borrow_mut(),
        "quake map",
        &*sbsts.physfs(),
    );*/
    utils::ss_map::ss_map_to_scene_parent(
        &rqmap::parser::ss::Entities::parse(&mut rqmap::parser::scanner::PeekScanner::new(rqmap::parser::scanner::Scanner::new(&mut BufReader::new(
            File::open("/mnt/big/archdata/cose/sst2/Serious-Engine/1_1_Palenque_EXPORT/Content/Engine110Export/Levels/LevelsMP/1_1_Palenque/1_1_Palenque.awf").unwrap(),
        ))))
        .unwrap(),
        sbsts.get_vulkan_common().unwrap(),
        &mut *sbsts.world.borrow_mut(),
        "ss map",
        &*sbsts.physfs(),
    );

    let audio_cell = sbsts.audio.clone();
    let audior = audio_cell.borrow_mut();
    let mut audio = audior.renderer.0.lock().unwrap();
    let mut player = player::Player::new(
        sbsts.get_vulkan_common().unwrap(),
        &mut *sbsts.world.borrow_mut(),
        sbsts
            .systems_manager
            .get_system_mut::<CameraSystem>()
            .unwrap(),
        &*sbsts.physfs(),
        &mut *audio,
        &mut UuidGenrator::new(seed),
        true,
    );
    drop(audio);
    drop(audior);

    physics::test(&mut sbsts);

    setup_input(&mut sbsts);

    let mut camera_rotation = Vector2::new(0.0f32, 0.0f32);

    let scene = std::env::args()
        .nth(1)
        .unwrap_or("demo.gkw.json".to_owned());
    println!("loading scene {scene}");

    sbsts.load(&scene);

    let res = {
        let s = sbsts.window.inner_size();
        [s.width, s.height]
    };
    let mut editor_ui = EditorUi::new(res);

    let mut prevt = std::time::Instant::now();
    //main loop
    let mut time: f32 = 0.0;
    let mut desired = 60.0;
    let mut should_run = true;
    let vk = sbsts.get_vulkan_common().unwrap();

    let mut game_ui = game_ui::GameUi::new();

    player::pickup::add_pickup(&mut *sbsts.world.borrow_mut(), vk.clone(), &*sbsts.physfs());
    enemy::add_enemy(&mut *sbsts.world.borrow_mut(), vk.clone(), &*sbsts.physfs());

    scripting::test(&sbsts);
    scripting::create_component_classes(sbsts.wren_scripting_engine.clone());

    let wren_vm = sbsts.wren_scripting_engine.clone();
    let mut script_ui = scripting::CodeUi::new();

    let offscreen_tetxure_id = sbsts
        .gk_egui
        .painter
        .insert_vulkan_image(sbsts.offscreen_output.image.clone())
        .unwrap();
    editor_ui.state.viewport_tid = offscreen_tetxure_id;

    let physfs = {
        let fs = sbsts.physfs();
        &*fs as *const game_kernel::physfs_rs::PhysFs
    };

    let mut framec = 0u64;

    sbsts.main_loop(
        move |delta,
              timestamp,
              mouse_pos,
              buttons,
              world,
              ui,
              user_events,
              manager,
              gkegui,
              captured| {
            let mut physics_system = manager.get_system_mut::<physics::PhysicsSystem>().unwrap();
            let input = &*manager.get_resource::<Input>();

            /*let (toggle_captured, touch_delta) = dbg_ui
                .state
                .touch_input_ui(gkegui, user_events);
            if toggle_captured {*captured = !*captured;}
            camera_rotation += touch_delta / 150f32;*/

            camera_rotation += mouse_pos / 1000f32;
            camera_rotation.y = camera_rotation.y.min(3.1415 / 2.0).max(-3.1415 / 2.0);

            if framec == 1 {
                player
                    .player_physics
                    .warp(&mut *world.borrow_mut(), &mut *physics_system);
            }
            framec += 1;
            //flycam(&world, camera_entity, delta, camera_rotation, input);
            let audio = audio_cell.borrow_mut();
            let mut audio = audio.renderer.0.lock().unwrap();
            player.update(
                Some(camera_rotation),
                &mut *world.borrow_mut(),
                input,
                physics_system,
                &mut *audio,
                delta / 1000.0,
            );

            let elapsed = prevt.elapsed().as_micros();
            script_ui.run_script(wren_vm.clone(), &gkegui.context(), world.clone());

            if !*captured {
                script_ui.script_ui(wren_vm.clone(), &gkegui.context(), world.clone());
                editor_ui
                    .state
                    .frames_ui(gkegui, delta, elapsed as f32 / 1000.0, &mut desired);
                editor_ui
                    .state
                    .dbg_ui(gkegui, world.borrow_mut(), &mut should_run);
                editor_ui.state.file_tree(gkegui, unsafe { &*physfs });
                editor_ui.draw_tabs(&gkegui.context, &mut *world.borrow_mut());

                if let (Some(selection), None) = (
                    editor_ui.state.viewport_select.take(),
                    editor_ui.state.pos_delta,
                ) {
                    let mesh_id = manager
                        .get_system_mut::<game_kernel::RendererSystem>()
                        .unwrap()
                        .get_renderer()
                        .borrow()
                        .fetch_mesh_id(selection);

                    let entity_id = world
                        .borrow()
                        .query::<(
                            game_kernel::utils::KeyType,
                            &game_kernel::StaticMeshComponent,
                        )>()
                        .find(|(_, mesh)| {
                            mesh.mesh_index()
                                .map(|i| i as u32 == mesh_id)
                                .unwrap_or(false)
                        })
                        .map(|(mesh_index, _)| mesh_index);
                    if let Some(id) = entity_id {
                        editor_ui.state.selected_entity = Some(id);
                    }
                }

                {
                    let camera_entity = manager
                        .get_system_mut::<game_kernel::CameraSystem>()
                        .unwrap()
                        .get_active_camera(&*world.borrow());

                    if let Some(camera_entity) = camera_entity {
                        editor_ui.state.camera = world
                            .borrow()
                            .get_entity_component::<CameraComponent>(camera_entity)
                            .unwrap()
                            .camera;
                    }
                }

                manager
                    .get_system_mut::<game_kernel::RendererSystem>()
                    .unwrap()
                    .get_renderer()
                    .borrow_mut()
                    .set_viewport_size(editor_ui.state.viewport_size);
            } else {
                manager
                    .get_system_mut::<game_kernel::RendererSystem>()
                    .unwrap()
                    .get_renderer()
                    .borrow_mut()
                    .set_viewport_size([10000, 10000]);
                let screen_size = vk.get_config().resolution;
                let ctx = gkegui.context();
                game_ui.draw(&ctx, screen_size, &player);
                egui::containers::Area::new("test")
                    .anchor(egui::Align2::RIGHT_TOP, egui::vec2(-10.0, 10.0))
                    .show(&ctx, |ui| {
                        ui.label(
                            egui::RichText::new(format!("{:.2} ({:.2})", 1000.0 / delta, delta))
                                .color(egui::Color32::WHITE),
                        );
                        ui.label(
                            egui::RichText::new(format!(
                                "{:.2} ({:.2})",
                                1000000.0 / elapsed as f32,
                                elapsed as f32 / 1000.0
                            ))
                            .color(egui::Color32::WHITE),
                        );
                        ui.painter().rect(
                            ui.min_rect().expand(6.0),
                            3.0,
                            egui::Color32::from_rgba_unmultiplied(30, 30, 30, 180),
                            egui::Stroke::none(),
                        );
                    });

                unsafe {
                    dbg_ui::SCREEN_CONSOLE
                        .as_ref()
                        .unwrap()
                        .lock()
                        .unwrap()
                        .draw(&gkegui.context(), delta / 1000.0);
                }
            };

            std::thread::sleep(Duration::from_micros(
                (1000000 / desired as i64 - elapsed as i64)
                    .max(1)
                    .min(100000) as u64,
            ));
            prevt = std::time::Instant::now();

            let mut world = world;
            /*anim.iter()
                .for_each(|a| a.play(&mut *world, (1.0 * time % 6000.0) / 1000.0));

            //skeletal anim
            let sframe = arm_anim
                .get_mut(0)
                .unwrap()
                .get_key_frame((1.0 * time % 6000.0) / 1000.0)
                .skeleton_frame();
            unsafe {
                if let Mesh::SkeletalMesh(ref mut mesh) = arm_mesh {
                    mesh.update_skeleton(&sframe, queue.clone());
                }
            }*/

            time += delta * 0.8;

            /*egui::Window::new("lol")
            .resizable(true)*/
            should_run
        },
    );

    println!("Bye");
}
