pub mod dbg_ui;
pub mod docking;
pub mod enemy;
pub mod game;
pub mod game_ui;
pub mod physics;
pub mod player;
pub mod scripting;
pub mod systems;
pub mod utils;

use cgmath;
use game_kernel::rodio::Source;
use game_kernel::vulkan::VulkanBasic;
use game_kernel::{self, CameraComponent, CameraSystem, TransformComponent};
use game_kernel_ecs;

use cgmath::*;
use egui;
use game_kernel::game_kernel::core_systems::{
    relative_transform::RelativeTransformComponent, tag::TagComponent,
};
use game_kernel::game_kernel::ecs::{AddEntity, EntitId, World};
use game_kernel::game_kernel::subsystems::video::ui::egui::GkEgui;
use game_kernel::game_kernel::subsystems::Subsystems;
use game_kernel::game_kernel::MainRenderer;
use game_kernel::renderer::{
    light::*, material::Material, mesh::*, model::Model, particle_fx::*, texture::*,
};

use std::any::Any;
use std::cell::RefCell;
use std::collections::HashMap;
use std::convert::TryFrom;
use std::fs::File;
use std::io::*;
use std::sync::Arc;
use std::time::Duration;

pub struct EguiTexture {
    id: egui::TextureId,
    size: [u32; 2],
}

use game_kernel::input::{Input, MouseKeys};
use std::ops::Deref;

fn setup_input(sbsts: &mut Subsystems) {
    use game_kernel::input::KeyTypes;
    sbsts.input.bind_action(KeyTypes::KeyBoard(17), "forward");
    sbsts.input.bind_action(KeyTypes::KeyBoard(31), "backward");
    sbsts.input.bind_control("forward", "forward", 1.0);
    sbsts.input.bind_control("backward", "forward", -1.0);

    sbsts.input.bind_action(KeyTypes::KeyBoard(32), "left");
    sbsts.input.bind_action(KeyTypes::KeyBoard(30), "right");
    sbsts.input.bind_control("left", "side", 1.0);
    sbsts.input.bind_control("right", "side", -1.0);

    sbsts.input.bind_action(KeyTypes::KeyBoard(57), "jump");

    sbsts.input.bind_action(KeyTypes::KeyBoard(2), "weapon1");
    sbsts.input.bind_action(KeyTypes::KeyBoard(3), "weapon2");
    sbsts.input.bind_action(KeyTypes::KeyBoard(4), "weapon3");
    sbsts.input.bind_action(KeyTypes::KeyBoard(5), "weapon4");
    sbsts
        .input
        .bind_action(KeyTypes::Mouse(MouseKeys::Left), "shoot");
    sbsts.input.bind_action(KeyTypes::KeyBoard(18), "pickup");
}

fn setup_flycam(sbsts: &mut game_kernel::game_kernel::subsystems::Subsystems) -> EntitId {
    //sets up camera
    let camera = game_kernel::Camera::new(
        Point3::new(0.0, 0.0, -6.0),
        Vector3::new(1.0, 0.0, 0.0),
        Vector3::new(0.0, -1.0, 0.0),
        Rad::turn_div_4() / 1f32,
        (0.1, 1000.0),
    );

    let camera_entity = AddEntity::new(sbsts.world.borrow_mut(), None)
        .unwrap()
        .with_component(game_kernel::CameraComponent::new(camera))
        .with_component(game_kernel::TransformComponent::identity())
        .entity;

    sbsts
        .systems_manager
        .get_system_mut::<game_kernel::CameraSystem>()
        .unwrap()
        .set_active_camera(camera_entity);

    camera_entity
}

fn flycam<W: Deref<Target = World>>(
    world: &W,
    camera_entity_index: EntitId,
    delta_time: f32,
    r: Vector2<f32>,
    input: &Input,
) {
    let mut camera = world
        .get_entity_components::<&mut CameraComponent>(camera_entity_index)
        .unwrap();

    camera.camera.dir =
        cgmath::Vector3::new(r.x.sin() * r.y.cos(), -r.y.sin(), r.x.cos() * r.y.cos());

    use cgmath::prelude::InnerSpace;
    let velocity = if input.is_action_down("sprint") {
        0.04
    } else {
        0.02
    };

    let camera = &mut camera.camera;
    camera.eye += camera.dir * delta_time * velocity * input.get_control_value("forward").unwrap();
    camera.eye += camera.dir.cross(camera.up).normalize()
        * delta_time
        * velocity
        * input.get_control_value(&"side".to_owned()).unwrap();

    let cp = camera.eye.to_vec();
    let cr = Matrix3::look_at(camera.dir, camera.up);
    let cr = Quaternion::from(cr.invert().unwrap());
    drop(camera);

    let mut c = world
        .get_entity_components::<&mut TransformComponent>(camera_entity_index)
        .unwrap();

    c.position = cp;
    c.rotation = cr;
}

pub fn game_main() {
    dbg_ui::init_screen_console();

    systems::register_components();
    player::pickup::register();

    let mut sbsts = game_kernel::game_kernel::subsystems::init().unwrap();

    sbsts
        .physfs
        .mount("./files", "/", false)
        .unwrap_or_else(|_| {});

    sbsts
        .physfs
        .mount("wad.pac", "/", false)
        .unwrap_or_else(|_| println!("failed to mount wad.pac"));

    sbsts
        .physfs
        .mount("sponza.pac", "/", false)
        .unwrap_or_else(|_| println!("failed to mount sponza.pac"));

    game_kernel::game_kernel::core_systems::init_systems(&mut sbsts);
    systems::init_systems(&mut sbsts.systems_manager, &mut *sbsts.world.borrow_mut());

    enemy::register_components();
    let vk = sbsts.get_vulkan_common().unwrap();
    enemy::init(
        &mut sbsts.systems_manager,
        &mut *sbsts.world.borrow_mut(),
        &sbsts.physfs,
        vk,
    );

    let renderer = sbsts.renderer.renderer.as_ref().unwrap().clone();

    /*let camera_entity = setup_flycam(&mut sbsts);
    let test_entity = AddEntity::new(sbsts.world.borrow_mut(), Some(camera_entity))
        .unwrap()
        .with_component(TagComponent::new("Relative test"))
        //.with_component(utils::load_test_cube(sbsts.get_vulkan_common().unwrap()))
        .with_component({
            use rapier3d::prelude::*;
            physics::ColliderComponent::from_collider(ColliderBuilder::cuboid(1.1, 1.1, 1.1).rotation(vector![0.0, 0.0, 0.0]).build())
        })
        .with_component(
            RelativeTransformComponent::identity()
                .translate(Vector3::new(-0.3, 0.4, 0.6))
                .rotate(Euler::new(Deg(0.0), Deg(0.0), Deg(180.0))),
        )
        .entity;*/

    /*utils::load_gltf(
        "files/weapons/shotgun/shotgun.glb",
        &mut *sbsts.world.borrow_mut(),
        test_entity,
        sbsts.get_vulkan_common().unwrap(),
    );*/

    let antique_camera = AddEntity::new(sbsts.world.borrow_mut(), None)
        .unwrap()
        .with_component(TagComponent::new("Antique Camera"))
        //.with_component(utils::load_test_cube(sbsts.get_vulkan_common().unwrap()))
        .with_component(RelativeTransformComponent::identity())
        .entity;

    utils::load_gltf_into_world(
        "AntiqueCamera.glb",
        &mut *sbsts.world.borrow_mut(),
        antique_camera,
        sbsts.get_vulkan_common().unwrap(),
        &sbsts.physfs,
    );

    utils::load_gltf_into_world(
        "grid.glb",
        &mut *sbsts.world.borrow_mut(),
        World::get_root(),
        sbsts.get_vulkan_common().unwrap(),
        &sbsts.physfs,
    );

    utils::load_gltf_into_world(
        "tube.glb",
        &mut *sbsts.world.borrow_mut(),
        World::get_root(),
        sbsts.get_vulkan_common().unwrap(),
        &sbsts.physfs,
    );

    /*let anim_entity = AddEntity::new(sbsts.world.borrow_mut(), Some(test_entity))
        .unwrap()
        .with_component(TagComponent::new("Anim test"))
        //.with_component(utils::load_test_cube(sbsts.get_vulkan_common().unwrap()))
        .with_component(
            RelativeTransformComponent::identity()
                .translate(Vector3::new(0.0, 0.0, 0.0))
                //.rotate(Euler::new(Deg(-90.0), Deg(0.0), Deg(-90.0))),
                .scale(Vector3::new(-3.0, 3.0, 3.0)),
        )
        .entity;
    let mut anim = utils::anim_test(
        //"files/simple-anim-test.glb",
        "files/weapons/shotgun/shotgun.glb",
        //"files/reltest3.glb",
        //"files/testrrotglb.glb",
        //"files/relativeScaleTest.glb",
        &mut *sbsts.world.borrow_mut(),
        anim_entity,
        sbsts.get_vulkan_common().unwrap(),
    );

    let (mut arm_anim, mut arm_mesh) = utils::skeletal_animation_test(
        "files/weapons/shotgun/shotgun.glb",
        sbsts.get_vulkan_common().unwrap(),
        &mut *sbsts.world.borrow_mut(),
        Some(anim_entity),
    );*/
    //let arm_anim = arm_anim.get_mut(0).unwrap();

    let audio_cell = sbsts.audio.clone();
    let audior = audio_cell.borrow_mut();
    let mut audio = audior.renderer.0.lock().unwrap();
    let mut player = player::Player::new(
        sbsts.get_vulkan_common().unwrap(),
        &mut *sbsts.world.borrow_mut(),
        sbsts
            .systems_manager
            .get_system_mut::<CameraSystem>()
            .unwrap(),
        &sbsts.physfs,
        &mut *audio,
    );
    drop(audio);
    drop(audior);

    physics::test(&mut sbsts);

    setup_input(&mut sbsts);

    let mut camera_rotation = Vector2::new(0.0f32, 0.0f32);

    println!("loading scene");

    sbsts.load("demo.gkw.json");

    let mut dbg_ui = dbg_ui::DbgUi::new();

    let mut prevt = std::time::Instant::now();
    //main loop
    let mut time: f32 = 0.0;
    let mut desired = 60.0;
    let mut should_run = true;
    let vk = sbsts.get_vulkan_common().unwrap();

    let mut game_ui = game_ui::GameUi::new();

    player::pickup::add_pickup(&mut *sbsts.world.borrow_mut(), vk.clone(), &sbsts.physfs);
    enemy::add_enemy(&mut *sbsts.world.borrow_mut(), vk.clone(), &sbsts.physfs);

    scripting::test(&sbsts);
    scripting::create_component_classes(sbsts.wren_scripting_engine.clone());

    let wren_vm = sbsts.wren_scripting_engine.clone();
    let mut script_ui = scripting::CodeUi::new();

    let offscreen_tetxure_id = sbsts
        .gk_egui
        .painter
        .insert_vulkan_image(sbsts.offscreen_output.image.clone())
        .unwrap();
    dbg_ui.state.viewport_tid = offscreen_tetxure_id;

    let physfs = &sbsts.physfs as *const game_kernel::physfs_rs::PhysFs;
    sbsts.main_loop(
        move |delta, mouse_pos, buttons, world, ui, input, user_events, manager, gkegui, captured| {
            let physics_system = manager.get_system_mut::<physics::PhysicsSystem>().unwrap();

            /*let (toggle_captured, touch_delta) = dbg_ui
                .state
                .touch_input_ui(gkegui, user_events);
            if toggle_captured {*captured = !*captured;}
            camera_rotation += touch_delta / 150f32;*/

            camera_rotation += mouse_pos / 1000f32;
            camera_rotation.y = camera_rotation.y.min(3.1415 / 2.0).max(-3.1415 / 2.0);

            //flycam(&world, camera_entity, delta, camera_rotation, input);
            let audio = audio_cell.borrow_mut();
            let mut audio = audio.renderer.0.lock().unwrap();
            player.update(
                camera_rotation,
                &mut *world.borrow_mut(),
                input,
                physics_system,
                &mut *audio,
                delta / 1000.0,
            );

            let elapsed = prevt.elapsed().as_micros();
            script_ui.run_script(wren_vm.clone(), &gkegui.context(), world.clone());

            if !*captured {
                script_ui.script_ui(wren_vm.clone(), &gkegui.context(), world.clone());
                dbg_ui
                    .state
                    .frames_ui(gkegui, delta, elapsed as f32 / 1000.0, &mut desired);
                dbg_ui
                    .state
                    .dbg_ui(gkegui, world.borrow_mut(), &mut should_run);
                dbg_ui.state.file_tree(gkegui, unsafe { &*physfs });
                dbg_ui.draw_tabs(&gkegui.context, &mut *world.borrow_mut());

                manager
                    .get_system_mut::<game_kernel::RendererSystem>()
                    .unwrap()
                    .get_renderer()
                    .borrow_mut()
                    .set_viewport_size(dbg_ui.state.viewport_size);
            } else {
                manager
                    .get_system_mut::<game_kernel::RendererSystem>()
                    .unwrap()
                    .get_renderer()
                    .borrow_mut()
                    .set_viewport_size([10000, 10000]);
                let screen_size = vk.get_config().resolution;
                let ctx = gkegui.context();
                game_ui.draw(&ctx, screen_size, &player);
                egui::containers::Area::new("test")
                    .fixed_pos(egui::pos2(screen_size[0] as f32 - 100.0, 10.0))
                    .show(&ctx, |ui| {
                        ui.label(format!("{:.2} ({:.2})", 1000.0 / delta, delta));
                        ui.label(format!(
                            "{:.2} ({:.2})",
                            1000000.0 / elapsed as f32,
                            elapsed as f32 / 1000.0
                        ));
                    });

                unsafe {
                    dbg_ui::SCREEN_CONSOLE
                        .as_ref()
                        .unwrap()
                        .lock()
                        .unwrap()
                        .draw(&gkegui.context(), delta / 1000.0);
                }
            };

            std::thread::sleep(Duration::from_micros(
                (1000000 / desired as i64 - elapsed as i64)
                    .max(1)
                    .min(100000) as u64,
            ));
            prevt = std::time::Instant::now();

            let mut world = world;
            /*anim.iter()
                .for_each(|a| a.play(&mut *world, (1.0 * time % 6000.0) / 1000.0));

            //skeletal anim
            let sframe = arm_anim
                .get_mut(0)
                .unwrap()
                .get_key_frame((1.0 * time % 6000.0) / 1000.0)
                .skeleton_frame();
            unsafe {
                if let Mesh::SkeletalMesh(ref mut mesh) = arm_mesh {
                    mesh.update_skeleton(&sframe, queue.clone());
                }
            }*/

            time += delta * 0.8;

            /*egui::Window::new("lol")
            .resizable(true)*/
            should_run
        },
    );

    println!("Bye");
}
