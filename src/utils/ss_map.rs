use std::{collections::HashMap, fs::File, io::BufReader, sync::Arc};

use cgmath::{vec2, vec3, Deg, Euler, Quaternion, Rotation3, Vector3};
use egui::epaint::ahash::HashSet;
use game_kernel::{
    core_systems::{
        physics::{cgna_v3, ColliderComponent, RigidBodyComponent},
        relative_transform::{RelativeTransformComponent, TransformInheritFlags},
        tag::TagComponent,
    },
    ecs::{AddEntity, EntitId, EntityId, World},
    physfs_rs::PhysFs,
    rapier3d::prelude::ColliderBuilder,
    renderer::{
        light::{Light, PointLight},
        mesh::{MeshData, StaticMesh},
        texture::{Texture, TextureData, TextureUsage},
    },
    vulkan::{VulkanBasic, VulkanCommon},
    LightComponent, Mesh, StaticMeshComponent, TransformComponent,
};
use rqmap::{
    self,
    parser::{
        scanner::{PeekScanner, Scanner},
        ss::{Parse, Property},
    },
};

use game_kernel::rapier3d::prelude::*;

use crate::player::{
    pickup::{AmmoPickupComponent, PickUppable},
    PlayerStartComponent,
};

use super::ImportedScene;

pub fn parse_quake_vec(s: &str) -> Option<Vector3<f32>> {
    let mut i = s.split(" ");
    Some(vec3(
        i.next()?.parse().ok()?,
        i.next()?.parse().ok()?,
        i.next()?.parse().ok()?,
    ))
}

pub fn quake_entity_transform(properties: &HashMap<String, String>) -> RelativeTransformComponent {
    let mut transform_component = RelativeTransformComponent::identity();
    if let Some(v) = properties.get("origin").and_then(|s| parse_quake_vec(s)) {
        transform_component = transform_component.translate(v);
    }

    transform_component
}

fn model_path(id: u32) -> String {
    format!("/mnt/big/archdata/cose/sst2/Serious-Engine/1_1_Palenque_EXPORT/Content/Engine110Export/Levels/LevelsMP/1_1_Palenque/1_1_Palenque_{id}.amf")
}

pub fn ss_map_to_scene(
    entities: &rqmap::parser::ss::Entities,
    vk: VulkanCommon,
    world: &mut World,
    map_parent: Option<EntitId>,
    fs: &PhysFs,
) {
    let mut imported_scene = ImportedScene::new();
    /*for entity in map.map().entities.iter() {
        if entity.brushes.len() > 0 {
            let entity = AddEntity::new(world, parent)
                .unwrap()
                .with_component(RelativeTransformComponent::identity());
            let
        }
    }*/

    let mut entity_map: HashMap<u32, EntityId> = HashMap::new();
    for entity in entities.entities.iter() {
        match &entity.properties["ENTITYCLASS"] {
            Property::String(class) if class == "SS1 WorldBase" => {
                let id = *entity.properties["ID"].unwrap_number() as u32;
                let parent = *entity.properties["PARENT"].unwrap_number() as u32;
                let name = entity.properties["NAME"].unwrap_string();
                let pos = entity.properties["POS"].unwrap_float3();
                let rot = entity.properties["ROT"].unwrap_float3();

                let transform_component = RelativeTransformComponent::identity()
                    .translate(vec3(pos.x as f32, pos.y as f32, pos.z as f32))
                    .rotate(Euler::new(
                        Deg(rot.x as f32),
                        Deg(rot.x as f32),
                        Deg(rot.x as f32),
                    ));

                let parent = entity_map.get(&parent).or(map_parent.as_ref());
                let new_node = AddEntity::new(&mut *world, parent.copied())
                    .unwrap()
                    .with_component(transform_component.clone())
                    .with_component(TagComponent::new(name))
                    .entity;
                entity_map.insert(id, new_node);

                let model_path = model_path(id);
                let file = File::open(model_path);
                match file {
                    Ok(file) => {
                        let stream = BufReader::new(file);
                        let mesh = rqmap::parser::ss::SeMesh::parse(&mut PeekScanner::new(
                            Scanner::new(stream),
                        ))
                        .unwrap();
                        mesh.layers
                            .iter()
                            .filter(|layer| layer.polygon_maps.is_some())
                            .for_each(|layer| {
                                let [mut positions, mut uvs] = [None; 2];
                                layer.vertex_maps.iter().for_each(|vertex_map| {
                                    match vertex_map.name.as_str() {
                                        "morph.position" => positions = Some(&vertex_map.elements),
                                        "texcoord.Texture 1" => uvs = Some(&vertex_map.elements),
                                        _ => {}
                                    }
                                });
                                if !positions.is_some() || !uvs.is_some() {
                                    return;
                                }
                                let [positions, uvs] = [positions, uvs].map(Option::unwrap);
                                let mut vert_positions = vec![];
                                let mut vert_uvs = vec![];
                                layer.vertices[0].vertices.iter().for_each(|vertex| {
                                    vert_positions.push(positions[vertex[0]]);
                                    vert_uvs.push(uvs[vertex[1]]);
                                });
                                let polygon_maps = layer.polygon_maps.as_ref().unwrap();
                                polygon_maps.polygon_maps.iter().for_each(|polygon_map| {
                                    let indices = &polygon_map.polygons;
                                    let mut index_map = HashMap::new();
                                    let (mut mesh_positions, mut mesh_uvs, mut mesh_norms) =
                                        (vec![], vec![], vec![]);
                                    indices.iter().for_each(|index| {
                                        if !index_map.contains_key(index) {
                                            index_map.insert(index, mesh_positions.len());
                                            mesh_positions.push(vec3(
                                                vert_positions[*index].x as f32,
                                                vert_positions[*index].y as f32,
                                                vert_positions[*index].z as f32,
                                            ));
                                            mesh_uvs.push(vec2(
                                                vert_uvs[*index].x as f32,
                                                vert_uvs[*index].y as f32,
                                            ));
                                            mesh_norms.push(vec3(0.0, 1.0, 0.0));
                                        }
                                    });

                                    let mesh_data = MeshData {
                                        vertex_buffer: mesh_positions.into_iter().collect(),
                                        normal_buffer: Some(mesh_norms.into_iter().collect()),
                                        uv_buffers: vec![mesh_uvs.into_iter().collect()],
                                        index_buffer: indices
                                            .iter()
                                            .map(|i| index_map[i] as u32)
                                            .collect(),
                                        face_direction:
                                            game_kernel::renderer::mesh::FaceDirection::ClockWise,
                                        cull_mode: game_kernel::renderer::mesh::CullMode::Back,
                                    };

                                    let mesh = StaticMesh::from_mesh_data(vk.clone(), &mesh_data);

                                    AddEntity::new(&mut *world, Some(new_node))
                                        .unwrap()
                                        .with_component(RelativeTransformComponent::identity())
                                        .with_component(TagComponent::new(format!("mesh_{id}")))
                                        .with_component(StaticMeshComponent::new(
                                            Arc::new(super::test_material(vk.clone(), None)),
                                            Arc::new(Mesh::StaticMesh(mesh)),
                                        ))
                                        .entity;
                                });
                            });
                    }
                    Err(e) => println!("failed to open file {e:?}"),
                }
            }
            // Some("info_player_start") => {
            //     AddEntity::new(&mut *world, parent)
            //         .unwrap()
            //         .with_component(quake_entity_transform(&entity.properties))
            //         .with_component(PlayerStartComponent::new())
            //         .with_component(TagComponent::new("Player start"));
            // }
            // Some("light") | Some("light_fluoro") => {
            //     AddEntity::new(&mut *world, parent)
            //         .unwrap()
            //         .with_component(quake_entity_transform(&entity.properties))
            //         .with_component(LightComponent::new(Light {
            //             light: game_kernel::renderer::light::LightType::Point(PointLight {
            //                 color: vec3(1.0, 1.0, 1.0)
            //                     * entity
            //                         .properties
            //                         .get("light")
            //                         .and_then(|s| s.parse().ok())
            //                         .unwrap_or(1.0)
            //                     / 5.0,
            //                 position: vec3(0.0, 0.0, 0.0),
            //                 size: 1.0,
            //                 influence_radius: 20.0,
            //             }),
            //             shadow_map_size: None,
            //         }))
            //         .with_component(TagComponent::new("QLight"));
            // }
            // Some("item_shells") => {
            //     let shell_entity = add_shells(
            //         world,
            //         parent.unwrap_or(World::get_root()),
            //         fs,
            //         vk.clone(),
            //         quake_entity_transform(&entity.properties).with_inherit_flags(
            //             TransformInheritFlags {
            //                 scale: true,
            //                 ..Default::default()
            //             },
            //         ),
            //     );
            //     world.add_component(shell_entity, TagComponent::new("Shells"));
            // }
            _ => {}
        }
    }

    // let partioned = map.partioned();
    // for (name, brush_mesh) in partioned.iter() {
    //     let albedo = map.wad().mip_texture(&name.to_lowercase()).map(|t| {
    //         let texture_data = TextureData {
    //             gl_internal_format: 35907, //TODO dont waste the alpha channel
    //             size: [t.width, t.height],
    //             is_cubemap: false,
    //             mip_levels: 1,
    //             layers_data: vec![t
    //                 .to_truecolor(0)
    //                 .chunks_exact(3)
    //                 .flat_map(|p| [p[0], p[1], p[2], 255])
    //                 .collect()],
    //         };
    //         Texture::from_texture_data(&texture_data, TextureUsage::Color, vk.clone(), None)
    //             .unwrap()
    //     });
    //     if brush_mesh.indices.len() == 0 {
    //         continue;
    //     }
    //     let mesh_data = MeshData {
    //         vertex_buffer: brush_mesh
    //             .vertices
    //             .iter()
    //             .map(|v| vec3(v.x as f32, v.y as f32, v.z as f32))
    //             .collect(),
    //         normal_buffer: Some(
    //             brush_mesh
    //                 .normals
    //                 .iter()
    //                 .map(|v| vec3(v.x as f32, v.y as f32, v.z as f32))
    //                 .collect(),
    //         ),
    //         uv_buffers: vec![brush_mesh
    //             .uvs
    //             .iter()
    //             .map(|(u, v)| vec2(*u as f32, *v as f32))
    //             .collect()],
    //         index_buffer: brush_mesh.indices.iter().cloned().collect(),
    //         face_direction: game_kernel::renderer::mesh::FaceDirection::ClockWise,
    //         cull_mode: game_kernel::renderer::mesh::CullMode::Back,
    //     };

    //     let mesh = StaticMesh::from_mesh_data(vk.clone(), &mesh_data);
    //     let _entity = AddEntity::new(&mut *world, parent)
    //         .unwrap()
    //         .with_component(RelativeTransformComponent::identity())
    //         .with_component(StaticMeshComponent::new(
    //             Arc::new(super::test_material(vk.clone(), albedo)),
    //             Arc::new(Mesh::StaticMesh(mesh)),
    //         ))
    //         .with_component(RigidBodyComponent::from_rigid_body(RigidBodyBuilder::new(RigidBodyType::Fixed).build()))
    //         .with_component(ColliderComponent::from_collider(
    //             ColliderBuilder::trimesh(
    //                 brush_mesh
    //                     .vertices
    //                     .iter()
    //                     .map(|v| point![-0.04 * v.x as f32, 0.04 * v.y as f32, 0.04 * v.z as f32])
    //                     .collect(),
    //                 brush_mesh
    //                     .indices
    //                     .chunks_exact(3)
    //                     .map(|s| [s[0], s[1], s[2]])
    //                     .collect(),
    //             )
    //             //.user_data(69420)
    //             .build(),
    //         ));
    // }
}

pub fn ss_map_to_scene_parent(
    map: &rqmap::parser::ss::Entities,
    vk: VulkanCommon,
    world: &mut World,
    parent_name: &str,
    fs: &PhysFs,
) {
    let parent = AddEntity::new(&mut *world, None)
        .unwrap()
        .with_component(TagComponent::new(parent_name))
        .with_component(
            RelativeTransformComponent::identity()
                .translate(vec3(0.0, 300.0, 0.0))
                .scale(vec3(-0.04, 0.04, 0.04))
                .rotate(Quaternion::from_axis_angle(vec3(1.0, 0.0, 0.0), Deg(-90.0))),
        )
        .entity;
    ss_map_to_scene(map, vk, world, Some(parent), fs);
}

pub fn add_shells(
    world: &mut World,
    parent: EntitId,
    fs: &PhysFs,
    vk: VulkanCommon,
    transform_component: RelativeTransformComponent,
) -> EntitId {
    let entity = world.add_entity(parent).unwrap();
    world.add_component(
        entity,
        ColliderComponent::from_collider(
            ColliderBuilder::cuboid(0.5, 0.5, 0.5)
                .user_data(entity as u128)
                //.sensor(true)
                .build(),
        ),
    );
    world.add_component(
        entity,
        RigidBodyComponent::from_rigid_body(
            RigidBodyBuilder::new(RigidBodyType::Dynamic)
                .translation(cgna_v3(transform_component.position))
                .gravity_scale(3.0)
                .additional_mass(10.0)
                .build(),
        ),
    );
    world.add_component(entity, transform_component);
    world.add_component(entity, TagComponent::new("Pickup"));
    world.add_component(entity, AmmoPickupComponent::new(1, 2000));
    world.add_component(entity, PickUppable {});

    crate::utils::load_gltf_into_world("meshes/shells.glb", world, entity, vk.clone(), fs);

    entity
}
