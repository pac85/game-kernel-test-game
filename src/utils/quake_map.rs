use std::{collections::HashMap, sync::Arc};

use cgmath::{vec2, vec3, Deg, Quaternion, Rotation3, Vector3};
use game_kernel::{
    core_systems::{
        physics::{cgna_v3, ColliderComponent, RigidBodyComponent},
        relative_transform::{RelativeTransformComponent, TransformInheritFlags},
        tag::TagComponent,
    },
    ecs::{AddEntity, EntitId, World},
    physfs_rs::PhysFs,
    rapier3d::prelude::ColliderBuilder,
    renderer::{
        light::{Light, PointLight},
        mesh::{MeshData, StaticMesh},
        texture::{Texture, TextureData, TextureUsage},
    },
    vulkan::{VulkanBasic, VulkanCommon},
    LightComponent, Mesh, StaticMeshComponent, TransformComponent,
};
use rqmap;

use game_kernel::rapier3d::prelude::*;

use crate::player::{
    pickup::{AmmoPickupComponent, PickUppable},
    PlayerStartComponent,
};

use super::ImportedScene;

pub fn parse_quake_vec(s: &str) -> Option<Vector3<f32>> {
    let mut i = s.split(" ");
    Some(vec3(
        i.next()?.parse().ok()?,
        i.next()?.parse().ok()?,
        i.next()?.parse().ok()?,
    ))
}

pub fn quake_entity_transform(properties: &HashMap<String, String>) -> RelativeTransformComponent {
    let mut transform_component = RelativeTransformComponent::identity();
    if let Some(v) = properties.get("origin").and_then(|s| parse_quake_vec(s)) {
        transform_component = transform_component.translate(v);
    }

    transform_component
}

pub fn quake_map_to_scene(
    map: &rqmap::MapAndWad,
    vk: VulkanCommon,
    world: &mut World,
    parent: Option<EntitId>,
    fs: &PhysFs,
) {
    let mut imported_scene = ImportedScene::new();
    /*for entity in map.map().entities.iter() {
        if entity.brushes.len() > 0 {
            let entity = AddEntity::new(world, parent)
                .unwrap()
                .with_component(RelativeTransformComponent::identity());
            let
        }
    }*/
    for entity in map.map().entities.iter() {
        match entity.properties.get("classname").map(|s| s.as_str()) {
            Some("info_player_start") => {
                AddEntity::new(&mut *world, parent)
                    .unwrap()
                    .with_component(quake_entity_transform(&entity.properties))
                    .with_component(PlayerStartComponent::new())
                    .with_component(TagComponent::new("Player start"));
            }
            Some("light") | Some("light_fluoro") => {
                AddEntity::new(&mut *world, parent)
                    .unwrap()
                    .with_component(quake_entity_transform(&entity.properties))
                    .with_component(LightComponent::new(Light {
                        light: game_kernel::renderer::light::LightType::Point(PointLight {
                            color: vec3(1.0, 1.0, 1.0)
                                * entity
                                    .properties
                                    .get("light")
                                    .and_then(|s| s.parse().ok())
                                    .unwrap_or(1.0)
                                / 5.0,
                            position: vec3(0.0, 0.0, 0.0),
                            size: 1.0,
                            influence_radius: 20.0,
                        }),
                        shadow_map_size: None,
                    }))
                    .with_component(TagComponent::new("QLight"));
            }
            Some("item_shells") => {
                let shell_entity = add_shells(
                    world,
                    parent.unwrap_or(World::get_root()),
                    fs,
                    vk.clone(),
                    quake_entity_transform(&entity.properties).with_inherit_flags(
                        TransformInheritFlags {
                            scale: true,
                            ..Default::default()
                        },
                    ),
                );
                world.add_component(shell_entity, TagComponent::new("Shells"));
            }
            _ => {}
        }
    }

    let partioned = map.partioned();
    for (name, brush_mesh) in partioned.iter() {
        let albedo = map.wad().mip_texture(&name.to_lowercase()).map(|t| {
            let texture_data = TextureData {
                gl_internal_format: 35907, //TODO dont waste the alpha channel
                size: [t.width, t.height],
                is_cubemap: false,
                mip_levels: 1,
                layers_data: vec![t
                    .to_truecolor(0)
                    .chunks_exact(3)
                    .flat_map(|p| [p[0], p[1], p[2], 255])
                    .collect()],
            };
            Texture::from_texture_data(&texture_data, TextureUsage::Color, vk.clone(), None)
                .unwrap()
        });
        if brush_mesh.indices.len() == 0 {
            continue;
        }
        let mesh_data = MeshData {
            vertex_buffer: brush_mesh
                .vertices
                .iter()
                .map(|v| vec3(v.x as f32, v.y as f32, v.z as f32))
                .collect(),
            normal_buffer: Some(
                brush_mesh
                    .normals
                    .iter()
                    .map(|v| vec3(v.x as f32, v.y as f32, v.z as f32))
                    .collect(),
            ),
            uv_buffers: vec![brush_mesh
                .uvs
                .iter()
                .map(|(u, v)| vec2(*u as f32, *v as f32))
                .collect()],
            index_buffer: brush_mesh.indices.iter().cloned().collect(),
            face_direction: game_kernel::renderer::mesh::FaceDirection::ClockWise,
            cull_mode: game_kernel::renderer::mesh::CullMode::Back,
        };

        let mesh = StaticMesh::from_mesh_data(vk.clone(), &mesh_data);
        let _entity = AddEntity::new(&mut *world, parent)
            .unwrap()
            .with_component(RelativeTransformComponent::identity())
            .with_component(StaticMeshComponent::new(
                Arc::new(super::test_material(vk.clone(), albedo)),
                Arc::new(Mesh::StaticMesh(mesh)),
            ))
            .with_component(RigidBodyComponent::from_rigid_body(
                RigidBodyBuilder::new(RigidBodyType::Fixed).build(),
            ))
            .with_component(ColliderComponent::from_collider(
                ColliderBuilder::trimesh(
                    brush_mesh
                        .vertices
                        .iter()
                        .map(|v| point![-0.04 * v.x as f32, 0.04 * v.y as f32, 0.04 * v.z as f32])
                        .collect(),
                    brush_mesh
                        .indices
                        .chunks_exact(3)
                        .map(|s| [s[0], s[1], s[2]])
                        .collect(),
                )
                //.user_data(69420)
                .build(),
            ));
    }
}

pub fn quke_map_to_scene_parent(
    map: &rqmap::MapAndWad,
    vk: VulkanCommon,
    world: &mut World,
    parent_name: &str,
    fs: &PhysFs,
) {
    let parent = AddEntity::new(&mut *world, None)
        .unwrap()
        .with_component(TagComponent::new(parent_name))
        .with_component(
            RelativeTransformComponent::identity()
                .translate(vec3(0.0, 300.0, 0.0))
                .scale(vec3(-0.04, 0.04, 0.04))
                .rotate(Quaternion::from_axis_angle(vec3(1.0, 0.0, 0.0), Deg(-90.0))),
        )
        .entity;
    quake_map_to_scene(map, vk, world, Some(parent), fs);
}

pub fn add_shells(
    world: &mut World,
    parent: EntitId,
    fs: &PhysFs,
    vk: VulkanCommon,
    transform_component: RelativeTransformComponent,
) -> EntitId {
    let entity = world.add_entity(parent).unwrap();
    world.add_component(
        entity,
        ColliderComponent::from_collider(
            ColliderBuilder::cuboid(0.5, 0.5, 0.5)
                .user_data(entity as u128)
                //.sensor(true)
                .build(),
        ),
    );
    world.add_component(
        entity,
        RigidBodyComponent::from_rigid_body(
            RigidBodyBuilder::new(RigidBodyType::Dynamic)
                .translation(cgna_v3(transform_component.position))
                .gravity_scale(3.0)
                .additional_mass(10.0)
                .build(),
        ),
    );
    world.add_component(entity, transform_component);
    world.add_component(entity, TagComponent::new("Pickup"));
    world.add_component(entity, AmmoPickupComponent::new(1, 2000));
    world.add_component(entity, PickUppable {});

    crate::utils::load_gltf_into_world("meshes/shells.glb", world, entity, vk.clone(), fs);

    entity
}
