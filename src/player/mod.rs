pub mod pickup;
pub mod player_physics;
pub mod weapons;

use std::cell::RefMut;

use cgmath::{prelude::*, Quaternion};
use cgmath::{Vector2, Vector3};
use game_kernel::game_kernel::core_systems::physics::{cgna_quat, nacg_v3};
use game_kernel::{
    audio::{
        self,
        filters::{self, Upmix},
        SourceWrapper, StoredSource,
    },
    input::Input,
    physfs_rs::PhysFs,
    rodio::{self, Source},
    vulkan::VulkanBasic,
    CameraSystem, TransformComponent,
};
use game_kernel_ecs::ecs::{EntitId, World};

use game_kernel::rapier3d::prelude::*;

type EntityId = EntitId;

use crate::physics::RigidBodyComponent;
use crate::{
    enemy::{projectile::ProjectileComponent, EnemyComponent},
    physics::{ColliderComponent, PhysicsSystem},
};

pub struct Player {
    pub player_physics: player_physics::PlayerPhysics,
    pub weapon_manager: weapons::WeaponManager,
    health: i32,
    jump_sound: u64,
    picked_up_id: Option<EntityId>,
}

impl Player {
    pub fn new(
        vk: impl VulkanBasic,
        world: &mut World,
        camera_system: RefMut<CameraSystem>,
        fs: &PhysFs,
        audio: &mut audio::Renderer,
    ) -> Self {
        let jump_sound = audio.add_non_spacial(SourceWrapper::new(StoredSource::from_source(
            Upmix::new(
                filters::Resample::new(
                    rodio::Decoder::new(fs.open_read("audio/Jump.wav").unwrap())
                        .unwrap()
                        .convert_samples(),
                    2,
                ),
                2,
            ),
            false,
        )));
        let player_physics =
            player_physics::PlayerPhysics::setup_player(vk.clone(), world, camera_system, fs);

        let weapon_manager = weapons::WeaponManager::new(
            player_physics.player_model_entity,
            player_physics.player_entity,
            player_physics.camera_entity,
            vk,
            fs,
            world,
        );

        Self {
            player_physics,
            weapon_manager,
            health: 100,
            jump_sound,
            picked_up_id: None,
        }
    }

    pub fn get_health(&self) -> i32 {
        self.health
    }

    pub fn melee_damage(
        &mut self,
        mut physics: &mut PhysicsSystem,
        player_entity: EntitId,
        world: &mut World,
        delta_time: f32,
    ) {
        let player_transform = world
            .get_entity_components::<&TransformComponent>(player_entity)
            .unwrap()
            .clone();

        physics.query_pipeline.intersections_with_shape(
            &physics.rigid_body_set,
            &physics.collider_set,
            &Isometry::translation(
                player_transform.position.x,
                player_transform.position.y,
                player_transform.position.z,
            ),
            &Capsule::new_y(0.6, 0.6),
            QueryFilter::new(),
            |collider_handle| {
                let collider = &physics.collider_set[collider_handle];
                let contact_entity = collider.user_data as EntityId;
                if world.has_components::<&EnemyComponent>(contact_entity) {
                    let mut enemy_component = world
                        .get_entity_components::<&mut EnemyComponent>(contact_entity)
                        .unwrap();
                    if enemy_component.cooldown <= 0.0 {
                        enemy_component.cooldown = 1.0;
                        self.health -= enemy_component.melee as i32;
                        return false;
                    }
                    enemy_component.cooldown -= delta_time;
                } else if world.has_components::<&ProjectileComponent>(contact_entity) {
                    self.health -= world
                        .get_entity_components::<&ProjectileComponent>(contact_entity)
                        .unwrap()
                        .damage;
                    world.rem_entity_recursive(contact_entity);
                }
                true
            },
        );
    }

    pub fn forward_ray(&self, world: &World) -> (Vector3<f32>, Vector3<f32>) {
        let player_collider_handle = world
            .get_entity_components::<&ColliderComponent>(self.player_physics.player_entity)
            .unwrap()
            .collider
            .unwrap_handle();

        let transform = world
            .get_entity_components::<&TransformComponent>(self.player_physics.camera_entity)
            .unwrap();
        (
            transform.position,
            transform
                .rotation
                .rotate_vector(cgmath::vec3(1.0, 0.0, 0.0)),
        )
    }

    pub fn pickup_pressed(
        &mut self,
        mut physics: &mut PhysicsSystem,
        player_entity: EntitId,
        world: &mut World,
    ) {
        let (mut position, forward) = self.forward_ray(world);
        position += forward * 1.0;

        physics
            .cast_ray(
                &Ray::new(
                    point![position.x, position.y, position.z],
                    crate::physics::cgna_v3(forward),
                ),
                4000.0,
                true,
                InteractionGroups::all(),
                None,
            )
            .and_then(move |(collider_handle, distance)| {
                let hit_entity = physics.collider_set[collider_handle].user_data as u64;
                if world.has_components::<(&pickup::PickUppable)>(hit_entity) {
                    self.picked_up_id = Some(hit_entity);
                    crate::screen_print!("picking up {hit_entity}");
                } else {
                    crate::screen_print!("can't pick up");
                }

                Some(1)
            });
    }

    pub fn picked_up_follow(
        &mut self,
        mut physics: &mut PhysicsSystem,
        player_entity: EntitId,
        world: &mut World,
        throw: bool,
    ) {
        if let Some(picked_up_id) = self.picked_up_id {
            let (mut position, forward) = self.forward_ray(world);
            position += forward * 3.0;

            if let Some((transform, rigid_body)) = world
                .get_entity_components::<(&mut TransformComponent, &mut RigidBodyComponent)>(
                    picked_up_id,
                )
            {
                let to_player = position - transform.position;

                //let go if too far
                if to_player.magnitude() > 10.0 {
                    self.picked_up_id = None;
                    return;
                }

                let rb: &mut RigidBody =
                    &mut physics.rigid_body_set[rigid_body.rigid_body.unwrap_handle()];
                if throw {
                    rb.reset_forces(false);
                    rb.lock_rotations(false, true);
                    rb.apply_impulse(crate::physics::cgna_v3(forward) * 300.0, true);
                    self.picked_up_id = None;
                } else {
                    if !rb.is_rotation_locked().iter().all(|x| *x) {
                        rb.lock_rotations(true, false);
                        rb.set_angvel(vector![0.0, 0.0, 0.0], true);
                    }
                    let force = 5000.0 * crate::physics::cgna_v3(to_player) - 600.0 * rb.linvel();
                    rb.reset_forces(false);
                    rb.add_force(force, true);
                }
            } else {
                self.picked_up_id = None;
            }
        }
    }

    pub fn picked_up_release(&mut self, physics: &mut PhysicsSystem, world: &mut World) {
        if let Some(picked_up_id) = self.picked_up_id {
            if let Some(rigid_body) =
                world.get_entity_components::<&mut RigidBodyComponent>(picked_up_id)
            {
                let rb: &mut RigidBody =
                    &mut physics.rigid_body_set[rigid_body.rigid_body.unwrap_handle()];
                rb.reset_forces(false);
                rb.lock_rotations(false, true);
                self.picked_up_id = None;
            } else {
                self.picked_up_id = None;
            }
        }
    }

    fn crazy_gravity(
        &mut self,
        mut physics: &mut PhysicsSystem,
        player_entity: EntitId,
        world: &mut World,
    ) {
        if let Some((mut transform, rigid_body)) = world
            .get_entity_components::<(&mut TransformComponent, &mut RigidBodyComponent)>(
                player_entity,
            )
        {
            let position = transform.position;
            let down = transform
                .rotation
                .rotate_vector(cgmath::vec3(0.0, -1.0, 0.0));

            physics
                .cast_ray_and_get_normal(
                    &Ray::new(
                        point![position.x, position.y, position.z],
                        crate::physics::cgna_v3(down),
                    ),
                    4000.0,
                    true,
                    InteractionGroups::all(),
                    Some(&|_, collider| {
                        collider.user_data == 69420
                    }),
                )
                .and_then(|(collider_handle, intersection)| {
                    /*let hit_entity = physics.collider_set[collider_handle].user_data as u64;
                    if world.has_components::<(&pickup::PickUppable)>(hit_entity) {
                    } else {
                    }*/
                    let rotation = Quaternion::between_vectors(
                        cgmath::vec3(0.0, 1.0, 0.0),
                        nacg_v3(&intersection.normal),
                    );
                    let rb: &mut RigidBody =
                        &mut physics.rigid_body_set[rigid_body.rigid_body.unwrap_handle()];
                    let desidered_rotation = cgna_quat(rotation);
                    rb.set_rotation(
                        rb.rotation().slerp(&desidered_rotation, 0.4).scaled_axis(),
                        false,
                    );
                    rb.reset_forces(false);
                    let acc = -(intersection.normal + vector![0.0, -1.0, 0.0])
                        * 9.81
                        * rb.gravity_scale();
                    rb.add_force(acc * rb.mass(), true);

                    Some(1)
                })
                .or_else(move || {
                    let rb: &mut RigidBody =
                        &mut physics.rigid_body_set[rigid_body.rigid_body.get_handle()?];
                    rb.reset_forces(false);
                    let desidered_rotation = game_kernel::rapier3d::na::Unit::<
                        game_kernel::rapier3d::na::Quaternion<f32>,
                    >::identity();
                    if rb.rotation().angle_to(&desidered_rotation) < 0.01 {
                        rb.set_rotation(desidered_rotation.scaled_axis(), true);
                    } else {
                        rb.set_rotation(
                            rb.rotation().slerp(&desidered_rotation, 0.2).scaled_axis(),
                            true,
                        );
                    }

                    Some(1)
                });
        }
    }

    pub fn update(
        &mut self,
        camera_rotation: Vector2<f32>,
        world: &mut World,
        input: &Input,
        mut physics: RefMut<PhysicsSystem>,
        audio: &mut audio::Renderer,
        delta_time: f32,
    ) {
        self.player_physics.mouse_look(camera_rotation, world);
        self.player_physics
            .movement(input, world, &mut *physics, delta_time);

        if self.player_physics.is_jumping() {
            audio
                .mutate_non_spacial(self.jump_sound, |source| {
                    source
                        .inner
                        .as_any_mut()
                        .downcast_mut::<StoredSource>()
                        .unwrap()
                        .set_current_sample(0);
                })
                .unwrap();
        }

        self.melee_damage(
            &mut *physics,
            self.player_physics.player_entity,
            world,
            delta_time,
        );

        self.crazy_gravity(&mut *physics, self.player_physics.player_entity, world);
        let pickup_triggered = input.is_action_triggered("pickup");
        let holding_something = self.picked_up_id.is_some();
        if pickup_triggered {
            self.picked_up_release(&mut *physics, world);
        }
        if pickup_triggered && !holding_something {
            self.pickup_pressed(&mut *physics, self.player_physics.player_entity, world);
        } else if input.is_action_triggered("shoot") {
            self.picked_up_follow(
                &mut *physics,
                self.player_physics.player_entity,
                world,
                true,
            );
        }
        self.picked_up_follow(
            &mut *physics,
            self.player_physics.player_entity,
            world,
            false,
        );

        self.weapon_manager
            .update(world, input, physics, audio, delta_time);
    }
}
