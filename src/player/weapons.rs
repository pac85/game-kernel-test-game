use std::{cell::RefMut, collections::HashMap, sync::Arc};

use cgmath::{vec3, InnerSpace, Matrix3, Quaternion, Rotation, Rotation3, Vector2, Vector3};
use game_kernel::rapier3d::prelude::Ray;
use game_kernel::{
    audio,
    game_kernel::core_systems::{
        particle_system::ParticleSystemComponent, relative_transform::RelativeTransformComponent,
        tag::TagComponent,
    },
    input::Input,
    physfs_rs::PhysFs,
    renderer::{
        particle_fx::{
            self, AlphaParticleRenderer, AlphaParticleRendererParams, DynCloneEmitter,
            DynamicParticleSystemBuilder, GenericParticleSystemBuilder, IntoLimitedEmitter,
            ParticleBlending, ParticleSystemBuilder, ParticleSystemBuilderWrapper, ParticleTexture,
            SimpleParticlePhysics, SphereEmitter,
        },
        texture::{Texture, TextureData, TextureUsage, SRGB},
    },
    vulkan::VulkanBasic,
    TransformComponent,
};
use game_kernel_ecs::ecs::{AddEntity, EntitId as EntityId, World};

use crate::{
    enemy::{follow::FollowComponent, EnemyComponent},
    physics::{self, cgna_v3, nacg_v3, ColliderComponent, PhysicsSystem, RigidBodyComponent},
    systems::self_destruct::{KillMode, SelfDestructComponent, SelfDestructTimer},
    utils::{load_gltf, ImportedScene, ImportedSkeletalAnimation},
};

pub struct Invenctory {
    pub ammo: HashMap<u8, u32>,
}

impl Invenctory {
    pub fn new() -> Self {
        Self {
            ammo: HashMap::new(),
        }
    }
}

pub trait Weapon {
    fn get_ammo_slot(&self) -> Option<u8> {
        None
    }
    fn ammo_per_shot(&self) -> u32 {
        1
    }
    fn show_fp_model(&self, world: &mut World, player_model_entity: EntityId);
    fn shoot(&self, world: &mut World, invenctory: &mut Invenctory, physics: RefMut<PhysicsSystem>);
}

pub struct DummyWeapon;
impl Weapon for DummyWeapon {
    fn show_fp_model(&self, world: &mut World, player_model_entity: EntityId) {}
    fn shoot(
        &self,
        world: &mut World,
        invenctory: &mut Invenctory,
        physics: RefMut<PhysicsSystem>,
    ) {
    }
}

pub struct WeaponManager {
    player_model_entity: EntityId,
    player_entity: EntityId,
    camera_entity: EntityId,
    invenctory: Invenctory,
    weapons: Vec<Box<dyn Weapon>>,
    current_weapon: usize,
    next_weapon: Option<usize>,
}

impl WeaponManager {
    pub fn new(
        player_model_entity: EntityId,
        player_entity: EntityId,
        camera_entity: EntityId,
        vk: impl VulkanBasic,
        fs: &PhysFs,
        world: &mut World,
    ) -> Self {
        let weapons = vec![
            Box::new(DummyWeapon) as _,
            Box::new(Shotgun::new(vk.clone(), fs, player_entity, camera_entity)) as _,
            Box::new(DoubleBarrelShotgun::new(
                vk.clone(),
                fs,
                player_entity,
                camera_entity,
            )) as _,
            Box::new(Torch::new(vk, fs, player_entity, camera_entity)) as _,
        ];

        Self {
            player_model_entity,
            player_entity,
            camera_entity,
            invenctory: Invenctory::new(),
            weapons,
            current_weapon: 0,
            next_weapon: None,
        }
    }

    fn try_swtiching_to_weapon(&mut self, weapon_slot: usize) {
        if self.current_weapon != weapon_slot && self.weapons.get(weapon_slot).is_some() {
            self.next_weapon = Some(weapon_slot);
        }
    }

    pub fn ammo_for_current(&self) -> u32 {
        let slot = self.weapons[self.current_weapon].get_ammo_slot();
        if slot.is_none() {
            return 0;
        }
        *self.invenctory.ammo.get(&slot.unwrap()).unwrap_or(&0)
    }

    pub fn update(
        &mut self,
        world: &mut World,
        input: &Input,
        physics: RefMut<PhysicsSystem>,
        audio: &mut audio::Renderer,
        delta_time: f32,
    ) {
        let player_transform: TransformComponent = world
            .get_entity_components::<&TransformComponent>(self.player_entity)
            .unwrap()
            .clone();
        physics.query_pipeline.intersections_with_shape(
            &physics.rigid_body_set,
            &physics.collider_set,
            &Isometry::translation(
                player_transform.position.x,
                player_transform.position.y,
                player_transform.position.z,
            ),
            &Capsule::new_y(0.6, 0.6),
            QueryFilter::new(),
            |collider_handle| {
                let collider = &physics.collider_set[collider_handle];
                let potential_pickup_entity = collider.user_data as EntityId;
                if world.has_components::<&AmmoPickupComponent>(potential_pickup_entity) {
                    let pickup_component = world
                        .get_entity_components::<&AmmoPickupComponent>(potential_pickup_entity)
                        .unwrap();
                    if let Some(ammo) = self.invenctory.ammo.get_mut(&pickup_component.slot) {
                        *ammo += pickup_component.quantity;
                    } else {
                        self.invenctory
                            .ammo
                            .insert(pickup_component.slot, pickup_component.quantity);
                    }
                    crate::screen_print!(
                        "picked up {} for slot {}",
                        pickup_component.quantity,
                        pickup_component.slot
                    );
                    drop(pickup_component);
                    world.rem_entity_recursive(potential_pickup_entity);
                }
                true
            },
        );

        if input.is_action_triggered("weapon1") {
            //self.remove_weapon_model(world);
            self.try_swtiching_to_weapon(0);
        }
        if input.is_action_triggered("weapon2") {
            //self.remove_weapon_model(world);
            self.try_swtiching_to_weapon(1);
        }
        if input.is_action_triggered("weapon3") {
            //self.remove_weapon_model(world);
            self.try_swtiching_to_weapon(2);
        }
        if input.is_action_triggered("weapon4") {
            //self.remove_weapon_model(world);
            self.try_swtiching_to_weapon(3);
        }
        if input.is_action_triggered("shoot") {
            let weapon = &self.weapons[self.current_weapon];
            if weapon.get_ammo_slot().is_none()
                || *self
                    .invenctory
                    .ammo
                    .get(&weapon.get_ammo_slot().unwrap())
                    .unwrap_or(&0)
                    > 0
            {
                weapon.shoot(world, &mut self.invenctory, physics);
                let ammo_per_shot = weapon.ammo_per_shot();
                &weapon
                    .get_ammo_slot()
                    .and_then(|slot| self.invenctory.ammo.get_mut(&slot))
                    .and_then(|v| {
                        *v -= ammo_per_shot;
                        Some(())
                    });
            }
        }

        if let Some(next_weapon) = self.next_weapon {
            self.remove_weapon_model(world);
            self.weapons[next_weapon].show_fp_model(world, self.player_model_entity);
            self.next_weapon = None;
            self.current_weapon = next_weapon;
        }
    }

    fn remove_weapon_model(&self, world: &mut World) {
        for child in world.get_children(self.player_model_entity).unwrap() {
            world.rem_entity_recursive(child).unwrap();
        }
    }
}

fn hitscan_pellet(
    world: &mut World,
    invenctory: &mut Invenctory,
    mut physics: &mut PhysicsSystem,
    ray: &Ray,
    player_collider_handle: ColliderHandle,
    sparks_pfx: Arc<ParticleSystemBuilderWrapper>,
) {
    let physics = &mut *physics;
    let mut world = &mut *world;

    physics
        .cast_ray(
            ray,
            4000.0,
            true,
            InteractionGroups::all(),
            Some(&|handle, _| player_collider_handle != handle),
        )
        .and_then(move |(collider_handle, distance)| {
            let hit_entity = physics.collider_set[collider_handle].user_data as u64;

            let mut dead = false;
            if let Some(mut entity_component) =
                world.get_entity_components::<&mut EnemyComponent>(hit_entity)
            {
                entity_component.health -= 8;
                dead = entity_component.health <= 0;
            }
            if dead {
                //world.rem_entity_recursive(hit_entity);
                world.remove_component::<EnemyComponent>(hit_entity);
                world.remove_component::<FollowComponent>(hit_entity);
                let rb_handle = physics.collider_set[collider_handle].parent().unwrap();
                let rb = &mut physics.rigid_body_set[rb_handle];
                rb.lock_rotations(false, true);
                rb.apply_impulse(10.0 * ray.dir, true);
            }

            let hitp = ray.origin + ray.dir * distance;
            //impulse to rigid bodies
            if let Some(rb_handle) = physics.collider_set[collider_handle].parent() {
                let rb = &mut physics.rigid_body_set[rb_handle];
                rb.apply_impulse_at_point(10.0 * ray.dir, hitp, true);
            }

            AddEntity::new(world, None)
                .unwrap()
                .with_component(TagComponent::new("Sparks"))
                .with_component(TransformComponent::identity().translate(nacg_v3(&hitp.coords)))
                .with_component(ParticleSystemComponent::new(sparks_pfx.clone()))
                .with_component(SelfDestructComponent::new(
                    SelfDestructTimer::Seconds(0.3),
                    KillMode::Recursive,
                ));

            Some(1)
        });
}

fn simple_sparks_pfx(vk: impl VulkanBasic, fs: &PhysFs) -> ParticleSystemBuilderWrapper {
    let spark_atlas = Texture::from_texture_data(
        &TextureData::load_from_kxt(fs.open_read("sparks.ktx").unwrap()).unwrap(),
        TextureUsage::Color,
        vk.clone(),
        Some(SRGB),
    )
    .unwrap();

    DynamicParticleSystemBuilder::new(
        Box::new(
            SphereEmitter::new(
                cgmath::Vector3::new(0.0, 0.0, 0.0),
                cgmath::Vector3::new(20.1, 20.1, 20.1),
                0.01,
                5000.0,
            )
            .limited(Some(1), None),
        )
        .wrapped(),
        Arc::new(Box::new(move |vk, output, depth, bufs| {
            let psr: Box<particle_fx::AlphaParticleRenderer> =
                Box::new(particle_fx::ParticlesRendererCreation::new(
                    vk,
                    output,
                    depth,
                    bufs,
                    particle_fx::AlphaParticleRendererParams {
                        life: 0.2,
                        size: Vector2::new(0.03, 0.03),
                        texture: ParticleTexture {
                            atlas: spark_atlas.clone(),
                            offset: Vector2::new(0.0, 0.0),
                            size: Vector2::new(1.0, 1.0),
                            fps: Vector2::new(10.0, 10.0 / 3.0),
                            frames: Vector2::new(1, 1),
                        },
                        blending: ParticleBlending::Additive,
                    },
                ));
            psr
        })),
        Arc::new(Box::new(|vk, max| {
            Box::new(SimpleParticlePhysics::new(
                vk,
                max,
                Vector3::new(0.0, -9.8, 0.0),
            ))
        })),
        10000,
    )
    .wrapped()
}

fn pellets_rotations(forward: Vector3<f32>, n: u32) -> impl Iterator<Item = Vector3<f32>> {
    let nforward = forward.normalize();
    let up = cgmath::vec3(0.0, 1.0, 0.0);
    let side = up.cross(nforward).normalize();

    (0..n)
        .flat_map(|a| (0..3).zip(std::iter::repeat(a)))
        .map(move |(r, alpha)| {
            let q = Quaternion::from_axis_angle(
                nforward,
                cgmath::Deg(alpha as f32 / (n as f32) * 360.0 + rand::random::<f32>()),
            ) * Quaternion::from_axis_angle(
                side,
                cgmath::Deg(r as f32 / 3.0 * 8.0 + rand::random::<f32>()),
            );
            q.rotate_vector(forward)
        })
}

struct Shotgun {
    player_entity: EntityId,
    camera_entity: EntityId,
    imported_arm: ImportedSkeletalAnimation,
    imported_shotgun: ImportedScene,
    sparks_pfx: Arc<ParticleSystemBuilderWrapper>,
}

impl Shotgun {
    pub fn new(
        vk: impl VulkanBasic,
        fs: &PhysFs,
        player_entity: EntityId,
        camera_entity: EntityId,
    ) -> Self {
        let path = "weapons/shotgun/shotgun.glb";
        let imported_arm = ImportedSkeletalAnimation::new(path, vk.clone(), fs);
        let imported_shotgun = load_gltf(path, vk.clone(), fs);

        let sparks_pfx = simple_sparks_pfx(vk, fs);

        Self {
            player_entity,
            camera_entity,
            imported_arm,
            imported_shotgun,
            sparks_pfx: Arc::new(sparks_pfx),
        }
    }
}

use game_kernel::rapier3d::na;
use game_kernel::rapier3d::prelude::*;

use super::pickup::AmmoPickupComponent;

impl Weapon for Shotgun {
    fn get_ammo_slot(&self) -> Option<u8> {
        Some(1)
    }

    fn show_fp_model(&self, world: &mut World, player_model_entity: EntityId) {
        self.imported_arm
            .load_into_world(world, Some(player_model_entity));
        self.imported_shotgun
            .load_into_world(world, player_model_entity);
    }

    fn shoot(
        &self,
        world: &mut World,
        invenctory: &mut Invenctory,
        mut physics: RefMut<PhysicsSystem>,
    ) {
        let player_collider_handle = world
            .get_entity_components::<&ColliderComponent>(self.player_entity)
            .unwrap()
            .collider
            .unwrap_handle();
        let (position, forward);
        {
            let transform = world
                .get_entity_components::<&TransformComponent>(self.camera_entity)
                .unwrap();
            position = transform.position;
            forward = transform
                .rotation
                .rotate_vector(cgmath::vec3(1.0, 0.0, 0.0));
        }
        pellets_rotations(forward, 5).for_each(|forward| {
            let physics = &mut *physics;
            let mut world = &mut *world;

            hitscan_pellet(
                world,
                invenctory,
                physics,
                &Ray::new(
                    point![position.x, position.y, position.z],
                    physics::cgna_v3(forward),
                ),
                player_collider_handle,
                self.sparks_pfx.clone(),
            );
        });
    }
}

struct DoubleBarrelShotgun {
    player_entity: EntityId,
    camera_entity: EntityId,
    imported_arm: ImportedSkeletalAnimation,
    imported_shotgun: ImportedScene,
    sparks_pfx: Arc<ParticleSystemBuilderWrapper>,
}

impl DoubleBarrelShotgun {
    pub fn new(
        vk: impl VulkanBasic,
        fs: &PhysFs,
        player_entity: EntityId,
        camera_entity: EntityId,
    ) -> Self {
        let path = "weapons/double_barrel_shotgun/double_barrel_shotgun.glb";
        let imported_arm = ImportedSkeletalAnimation::new(path, vk.clone(), fs);
        let imported_shotgun = load_gltf(path, vk.clone(), fs);

        let sparks_pfx = simple_sparks_pfx(vk, fs);

        Self {
            player_entity,
            camera_entity,
            imported_arm,
            imported_shotgun,
            sparks_pfx: Arc::new(sparks_pfx),
        }
    }
}

impl Weapon for DoubleBarrelShotgun {
    fn get_ammo_slot(&self) -> Option<u8> {
        Some(1)
    }

    fn show_fp_model(&self, world: &mut World, player_model_entity: EntityId) {
        self.imported_arm
            .load_into_world(world, Some(player_model_entity));
        self.imported_shotgun
            .load_into_world(world, player_model_entity);
    }

    fn shoot(
        &self,
        world: &mut World,
        invenctory: &mut Invenctory,
        mut physics: RefMut<PhysicsSystem>,
    ) {
        let player_collider_handle = world
            .get_entity_components::<&ColliderComponent>(self.player_entity)
            .unwrap()
            .collider
            .unwrap_handle();
        let (position, forward);
        {
            let transform = world
                .get_entity_components::<&TransformComponent>(self.camera_entity)
                .unwrap();
            position = transform.position;
            forward = transform
                .rotation
                .rotate_vector(cgmath::vec3(1.0, 0.0, 0.0));
        }
        pellets_rotations(forward, 5).for_each(|forward| {
            let physics = &mut *physics;
            let mut world = &mut *world;

            hitscan_pellet(
                world,
                invenctory,
                physics,
                &Ray::new(
                    point![position.x, position.y, position.z],
                    physics::cgna_v3(forward),
                ),
                player_collider_handle,
                self.sparks_pfx.clone(),
            );
        });
    }
}

struct Torch {
    player_entity: EntityId,
    camera_entity: EntityId,
    imported_arm: ImportedSkeletalAnimation,
    imported_shotgun: ImportedScene,
}

impl Torch {
    pub fn new(
        vk: impl VulkanBasic,
        fs: &PhysFs,
        player_entity: EntityId,
        camera_entity: EntityId,
    ) -> Self {
        let path = "weapons/double_barrel_shotgun/double_barrel_shotgun.glb";
        let imported_arm = ImportedSkeletalAnimation::new(path, vk.clone(), fs);
        let imported_shotgun = load_gltf(path, vk.clone(), fs);

        Self {
            player_entity,
            camera_entity,
            imported_arm,
            imported_shotgun,
        }
    }
}

use game_kernel::renderer::light::{Light, LightType, SpotLight};
use game_kernel::LightComponent;

impl Weapon for Torch {
    fn get_ammo_slot(&self) -> Option<u8> {
        Some(1)
    }

    fn show_fp_model(&self, world: &mut World, player_model_entity: EntityId) {
        self.imported_arm
            .load_into_world(world, Some(player_model_entity));
        self.imported_shotgun
            .load_into_world(world, player_model_entity);
        AddEntity::new(world, Some(player_model_entity))
            .unwrap()
            .with_component(LightComponent::new(Light {
                light: LightType::Spot(SpotLight {
                    color: vec3(1000.0, 1000.0, 1000.0),
                    position: vec3(0.0, 0.0, 0.0),
                    size: 1.0,
                    direction: vec3(0.0, 1.0, 0.0),
                    cone_length: 100.0,
                    cone_angle: 70.0,
                }),
                shadow_map_size: Some(512),
            }))
            .with_component(
                RelativeTransformComponent::identity()
                    .translate(vec3(0.0, 0.0, 0.4))
                    .rotate(cgmath::Quaternion::from_angle_x(cgmath::Deg(-90.0))),
            );
    }

    fn shoot(
        &self,
        world: &mut World,
        invenctory: &mut Invenctory,
        mut physics: RefMut<PhysicsSystem>,
    ) {
    }
}
