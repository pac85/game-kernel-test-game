use std::{
    cell::{RefCell, RefMut},
    ops::DerefMut,
    sync::Arc,
};

use cgmath::*;
use game_kernel::{
    game_kernel::{
        core_systems::{
            physics::cgna_v3, relative_transform::RelativeTransformComponent, tag::TagComponent,
        },
        ecs::{System, SystemManager},
        subsystems::Subsystems,
    },
    input::Input,
    physfs_rs::PhysFs,
    vulkan::VulkanBasic,
    CameraSystem, TransformComponent,
};
use game_kernel_ecs::ecs::*;

use crate::physics::{self, ColliderComponent, PhysicsSystem, RigidBodyComponent};

fn setup_camera(
    world: &mut World,
    mut camera_system: RefMut<CameraSystem>,
    parent: u64,
) -> EntitId {
    //sets up camera
    let camera = game_kernel::Camera::new(
        Point3::new(0.0, 0.0, -6.0),
        Vector3::new(1.0, 0.0, 0.0),
        Vector3::new(0.0, -1.0, 0.0),
        Rad::turn_div_4() / 1f32,
        (0.1, 1000.0),
    );

    let camera_entity = AddEntity::new(world, Some(parent))
        .unwrap()
        .with_component(game_kernel::CameraComponent::new(camera))
        .with_component(TagComponent::new("Camera"))
        .with_component(RelativeTransformComponent::identity().translate(vec3(0.0, 0.6, 0.0)))
        .entity;

    camera_system.set_active_camera(camera_entity);

    camera_entity
}

pub struct PlayerPhysics {
    pub player_entity: EntitId,
    pub player_model_entity: EntitId,
    pub camera_entity: EntitId,
    player_feet: EntitId,
    buffered_jump: bool,
    max_velocity: f32,
    friction: f32,
    prev_velocity: Vector<f32>,
    weapon_offset_accum: Vector<f32>,
    weapon_velocity: Vector<f32>,
    speed_integral: f32,
    bobbing_accum: Vector3<f32>,
    jumping: bool,
}

use game_kernel::rapier3d::prelude::*;

impl PlayerPhysics {
    pub fn setup_player(
        vk: impl VulkanBasic,
        world: &mut World,
        camera_system: RefMut<CameraSystem>,
        fs: &PhysFs,
    ) -> Self {
        let player_entity = AddEntity::new(&mut *world, None)
            .unwrap()
            .with_component(TagComponent::new("Player entity"))
            .with_component({
                use game_kernel::rapier3d::prelude::*;
                physics::RigidBodyComponent::from_rigid_body(
                    RigidBodyBuilder::new(RigidBodyType::Dynamic)
                        .translation(vector![0.0, 10.0, 0.0])
                        .lock_rotations()
                        .ccd_enabled(true)
                        .gravity_scale(6.0)
                        .build(),
                )
            })
            .with_component({
                use game_kernel::rapier3d::prelude::*;
                physics::ColliderComponent::from_collider(
                    ColliderBuilder::capsule_y(0.5, 0.5)
                        .mass(10.0)
                        //.active_events(ActiveEvents::CONTACT_EVENTS)
                        .build(),
                )
            })
            .with_component(TransformComponent::identity())
            .entity;

        let camera_entity = setup_camera(&mut *world, camera_system, player_entity);

        let player_model_entity = AddEntity::new(&mut *world, Some(camera_entity))
            .unwrap()
            .with_component(TagComponent::new("Player model"))
            //.with_component(utils::load_test_cube(sbsts.get_vulkan_common().unwrap()))
            .with_component(
                RelativeTransformComponent::identity()
                    .translate(Vector3::new(0.3, -0.4, -0.6))
                    .rotate(Euler::new(Deg(0.0), Deg(90.0), Deg(0.0)))
                    //.rotate(Euler::new(Deg(-90.0), Deg(0.0), Deg(-90.0))),
                    .scale(Vector3::new(-3.0, 3.0, 3.0)),
            )
            .entity;

        let player_feet = AddEntity::new(&mut *world, Some(player_entity))
            .unwrap()
            .with_component(TagComponent::new("Player feet"))
            //.with_component(utils::load_test_cube(sbsts.get_vulkan_common().unwrap()))
            /*.with_component({
                use rapier3d::prelude::*;
                physics::ColliderComponent::from_collider(
                    ColliderBuilder::cuboid(1.0, 0.1, 1.0)
                        .density(0.0)
                        .active_events(ActiveEvents::INTERSECTION_EVENTS)
                        .sensor(true)
                        .build(),
                )
            })*/
            .with_component(RelativeTransformComponent::identity().translate(vec3(0.0, -4.0, 0.0)))
            .entity;

        Self {
            player_entity,
            player_model_entity,
            camera_entity,
            player_feet,
            buffered_jump: false,
            max_velocity: 24.0, //should be 14.0,
            friction: 6.0,
            prev_velocity: vector![0.0, 0.0, 0.0],
            weapon_offset_accum: vector![0.0, 0.0, 0.0],
            weapon_velocity: vector![0.0, 0.0, 0.0],
            speed_integral: 0.0,
            bobbing_accum: Vector3::zero(),
            jumping: false,
        }
    }

    pub fn mouse_look(&self, camera_rotation: Vector2<f32>, world: &mut World) {
        let rotation = Euler::new(
            Deg(0.0),
            Rad(camera_rotation.x).into(),
            Rad(-camera_rotation.y).into(),
        );
        let rot = Quaternion::from(rotation);
        let mut transform = world
            .get_entity_components::<&mut RelativeTransformComponent>(self.camera_entity)
            .unwrap();

        transform.rotation = rot;
    }

    pub fn get_camera_rot(&self, world: &World) -> Quaternion<f32> {
        let transform = world
            .get_entity_components::<&RelativeTransformComponent>(self.camera_entity)
            .unwrap();

        transform.rotation
    }

    pub fn get_player_rot(&self, world: &World) -> Quaternion<f32> {
        let transform = world
            .get_entity_components::<&TransformComponent>(self.player_entity)
            .unwrap();

        transform.rotation
    }

    pub fn get_camera_dir(&self, world: &World) -> (Vector3<f32>, Vector3<f32>, Vector3<f32>) {
        (
            self.get_camera_rot(world)
                .rotate_vector(cgmath::vec3(1.0, 0.0, 0.0)),
            self.get_camera_rot(world)
                .rotate_vector(cgmath::vec3(0.0, 1.0, 0.0)),
            self.get_camera_rot(world)
                .rotate_vector(cgmath::vec3(0.0, 0.0, 1.0)),
        )
    }

    pub fn accellerate(
        &self,
        accellerate: f32,
        accel_dir: Vector<f32>,
        velocity: Vector<f32>,
        max_velocity: f32,
        delta_time: f32,
    ) -> Vector<f32> {
        let proj_vel = velocity.dot(&accel_dir);
        let mut accel_vel = accellerate * delta_time;
        if proj_vel + accel_vel > max_velocity {
            accel_vel = max_velocity - proj_vel;
        }

        velocity + (accel_dir * accel_vel.max(0.0))
    }

    pub fn move_ground(
        &self,
        accel_dir: Vector<f32>,
        velocity: Vector<f32>,
        delta_time: f32,
    ) -> Vector<f32> {
        let speed = velocity.norm().max(self.max_velocity / 5.0);
        let mut new_vel = velocity;
        if speed != 0.0 {
            let drop = speed * self.friction * delta_time;
            new_vel *= (speed - drop).max(0.0) / speed;
        }

        self.accellerate(640.0, accel_dir, new_vel, self.max_velocity, delta_time)
    }

    fn weapon_movement(
        &mut self,
        world: &mut World,
        rb: &RigidBody,
        touching_ground: bool,
        delta_time: f32,
    ) {
        let base_translation = Vector3::new(0.3, -0.4, -0.6);
        let camera_rot = self.get_camera_rot(world);
        let mut relative_transform = world
            .get_entity_components::<&mut RelativeTransformComponent>(self.player_model_entity)
            .unwrap();

        let accel = (rb.linvel() - self.prev_velocity) / delta_time;
        self.prev_velocity = *rb.linvel();
        self.speed_integral += self
            .prev_velocity
            .component_mul(&vector![1.0, 0.0, 1.0])
            .norm()
            * delta_time;

        let accel = physics::cgna_quat(camera_rot).transform_vector(&accel);

        //damped harmonic oscillator
        self.weapon_velocity += (-self.weapon_offset_accum * 60.1 + accel * 0.1) * delta_time;
        self.weapon_offset_accum += self.weapon_velocity * delta_time;
        self.weapon_velocity *= 0.7;
        let bobbing_frequency = 0.218;
        use std::ops::Mul;
        let bobbing = if touching_ground {
            vec3(
                0.0,
                -self.speed_integral.mul(bobbing_frequency).sin().abs(),
                0.7 * self.speed_integral.mul(bobbing_frequency).cos(),
            ) * 0.1
        } else {
            Vector3::zero()
        };

        self.bobbing_accum = self.bobbing_accum.lerp(bobbing, 0.07);

        relative_transform.position = base_translation
            + physics::nacg_v3(&self.weapon_offset_accum).mul_element_wise(vec3(0.5, 1.0, 0.5))
                * 0.8
            + self.bobbing_accum * 1.0;
    }

    pub fn movement(
        &mut self,
        input: &Input,
        world: &mut World,
        physics: &mut PhysicsSystem,
        delta_time: f32,
    ) {
        let (mut dir, up, side) = self.get_camera_dir(world);
        let player_rot = self.get_player_rot(world);

        dir.y = 0.0;
        dir = dir.normalize();
        dir = player_rot.rotate_vector(dir);

        let up = player_rot.rotate_vector(vec3(0.0, 1.0, 0.0));

        let force_dir = dir * input.get_control_value("forward").unwrap()
            + dir.cross(up).normalize() * -input.get_control_value(&"side".to_owned()).unwrap();

        let force_dir = if force_dir.magnitude() != 0.0 {
            force_dir.normalize()
        } else {
            vec3(0.0, 0.0, 0.0)
        };

        let rigid_body = world
            .get_entity_components::<&mut RigidBodyComponent>(self.player_entity)
            .unwrap()
            .rigid_body
            .get_handle();

        if let Some(handle) = rigid_body {
            let (collider, pos) = {
                let rb = physics.rigid_body_set.get_mut(handle).unwrap();
                (
                    rb.colliders()[0],
                    (rb.position() * Isometry::translation(0.0, -1.2, 0.0))
                )
            };

            let is_touching_ground = physics
                .query_pipeline
                .intersection_with_shape(
                    &physics.rigid_body_set,
                    &physics.collider_set,
                    &pos,
                    &Cuboid::new(vector![0.25, 0.1, 0.25]),
                    QueryFilter::new().predicate(&|h, _| h != collider),
                )
                .is_some();

            let rb = physics.rigid_body_set.get_mut(handle).unwrap();
            crate::screen_print!("is touching gr {is_touching_ground}");
            //let is_touching_ground = physics.contact_tracker.is_touching(collider);
            //self.accellerate(1000.0, crate::physics::cgna_v3(force_dir), *rb.linvel(), 1.0/60.0)
            let vel = if is_touching_ground {
                self.move_ground(crate::physics::cgna_v3(force_dir), *rb.linvel(), delta_time)
            } else {
                self.accellerate(
                    64.0,
                    crate::physics::cgna_v3(force_dir),
                    *rb.linvel(),
                    self.max_velocity / 1.0,
                    delta_time,
                )
            };

            self.weapon_movement(world, rb, is_touching_ground, delta_time);
            rb.set_linvel(vel, true);

            println!("\nspeed {}", rb.linvel().norm());

            self.buffered_jump |= input.is_action_triggered("jump");
            self.jumping = self.buffered_jump && input.is_action_down("jump") && is_touching_ground;
            if self.jumping {
                let compensation = -rb.linvel().y * rb.mass();
                rb.apply_impulse(cgna_v3(up) * (200.0 + compensation), true);
                self.buffered_jump = false;
            }
        }
    }

    pub fn is_jumping(&self) -> bool {
        self.jumping
    }
}
