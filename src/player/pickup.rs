use std::any::Any;
use std::collections::HashMap;
use std::sync::Arc;

use game_kernel::game_kernel::core_systems::tag::TagComponent;
use game_kernel::game_kernel::ecs::*;
use game_kernel::physfs_rs::PhysFs;
use game_kernel::rapier3d::prelude::Collider;
use game_kernel::vulkan::VulkanBasic;
use game_kernel::TransformComponent;
use game_kernel_ecs::ecs::entity;

pub fn register() {
    let mut component_factory = COMPONENT_FACTORY.lock().unwrap();
    component_factory.register::<AmmoPickupComponent>();
    component_factory.register::<WeaponPickupComponent>();
    component_factory.register::<PickUppable>();
}

#[derive(Clone, Copy, Component)]
pub struct AmmoPickupComponent {
    pub slot: u8,
    pub quantity: u32,
}

impl AmmoPickupComponent {
    pub fn new(slot: u8, quantity: u32) -> Self {
        Self { slot, quantity }
    }
}

#[derive(Clone, Copy, Component)]
pub struct WeaponPickupComponent {
    pub slot: u8,
}

impl WeaponPickupComponent {
    pub fn new(slot: u8) -> Self {
        Self { slot }
    }
}

#[derive(Clone, Copy, Component)]
pub struct PickUppable {}

use crate::physics::{ColliderComponent, RigidBodyComponent};
use game_kernel::rapier3d::prelude::*;

pub fn add_pickup(world: &mut World, vk: impl VulkanBasic, fs: &PhysFs) {
    let entity = world.add_entity(World::get_root()).unwrap();
    world.add_component(
        entity,
        ColliderComponent::from_collider(
            ColliderBuilder::cuboid(0.5, 0.5, 0.5)
                .user_data(entity as u128)
                //.sensor(true)
                .build(),
        ),
    );
    world.add_component(
        entity,
        TransformComponent::identity()
            .translate(cgmath::vec3(2.0, -9.5, 0.0))
            .scale(cgmath::vec3(5.0, 5.0, 5.0)),
    );
    world.add_component(entity, TagComponent::new("Pickup"));
    world.add_component(entity, AmmoPickupComponent::new(1, 20));
    world.add_component(
        entity,
        RigidBodyComponent::from_rigid_body(
            RigidBodyBuilder::new(RigidBodyType::Dynamic)
                .translation(vector![2.0, -9.0, 0.0])
                .gravity_scale(3.0)
                .additional_mass(10.0)
                .build(),
        ),
    );
    world.add_component(entity, PickUppable {});

    crate::utils::load_gltf_into_world("shells.glb", world, entity, vk.clone(), fs);

    let mut builder = AddEntity::new(&mut *world, None).unwrap();
    let entity = builder.entity;
    builder
        .with_component(ColliderComponent::from_collider(
            ColliderBuilder::cuboid(0.53, 0.53, 0.53)
                .rotation(vector![0.0, 0.0, 0.0])
                .user_data(entity as u128)
                .density(1.0)
                .build(),
        ))
        .with_component(RigidBodyComponent::from_rigid_body(
            RigidBodyBuilder::new(RigidBodyType::Dynamic)
                .translation(vector![0.0, -8.0, 0.0])
                .gravity_scale(3.0)
                .additional_mass(10.0)
                .build(),
        ))
        .with_component(
            TransformComponent::identity()
                .translate((0.0, -10.0, 0.0).into())
                .scale(cgmath::vec3(0.53, 0.53, 0.53)),
        )
        .with_component(PickUppable {})
        .with_component(TagComponent::new("Pickuppable"))
        .entity;

    crate::utils::load_gltf_into_world("cube.glb", world, entity, vk, fs);
}
