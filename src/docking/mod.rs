use egui::*;
use std::boxed::Box;

pub enum DockingDirection {
    Vertcial,
    Horizontal,
}

pub enum DockingElement {
    Area(Box<dyn FnOnce(&mut Ui)>),
    Tabbed(Vec<Self>),
    Docked((Vec<Self>, DockingDirection)),
}

pub struct DockingArea {
    root: DockingElement,
}

impl DockingArea {}
