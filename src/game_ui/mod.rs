use std::fmt::format;

use crate::player::Player;
use egui::{Align2, Color32, Label, RichText, Widget};

pub struct GameUi {}

impl GameUi {
    pub fn new() -> Self {
        Self {}
    }

    pub fn draw(&mut self, ctx: &egui::Context, screen_size: [u32; 2], player: &Player) {
        /*egui::containers::Area::new("player speed")
            .anchor(Align2::RIGHT_BOTTOM, egui::vec2(10.0, 10.0))
            .show(&ctx, |ui|{
                player.player_physics.
        });*/

        let cross_air_size = egui::vec2(20.0, 20.0);
        let cross_air_start = egui::pos2(screen_size[0] as f32 / 2.0, screen_size[1] as f32 / 2.0)
            - cross_air_size / 2.0;
        egui::containers::Area::new("cross air")
            .fixed_pos(cross_air_start)
            .show(&ctx, |ui| {
                let painter = ui.painter();
                let stroke = egui::Stroke::new(1.0, egui::Color32::GRAY);
                painter.line_segment(
                    [
                        cross_air_start + cross_air_size * egui::vec2(0.0, 0.5),
                        cross_air_start + cross_air_size * egui::vec2(1.0, 0.5),
                    ],
                    stroke,
                );
                painter.line_segment(
                    [
                        cross_air_start + cross_air_size * egui::vec2(0.5, 0.0),
                        cross_air_start + cross_air_size * egui::vec2(0.5, 1.0),
                    ],
                    stroke,
                );
            });

        egui::containers::Area::new("health")
            .anchor(Align2::LEFT_BOTTOM, egui::vec2(0.0, 0.0))
            .show(&ctx, |ui| {
                ui.horizontal(|ui| {
                    ui.label(RichText::new("❤").size(48.0).color(Color32::RED));
                    ui.label(
                        RichText::new(format!("{}", player.get_health()))
                            .size(48.0)
                            .color(Color32::WHITE),
                    );
                    ui.label(RichText::new("  ").size(48.0));
                    ui.label(RichText::new("📦").size(48.0).color(Color32::YELLOW));
                    ui.label(
                        RichText::new(format!("{}", player.weapon_manager.ammo_for_current()))
                            .size(48.0)
                            .color(Color32::WHITE),
                    );
                });
                //format!("{}", player.get_health())).text_style(egui::TextStyle::Heading).ui(ui);
            });
    }
}
