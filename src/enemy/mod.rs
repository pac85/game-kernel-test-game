use std::any::Any;
use std::collections::HashMap;
use std::sync::Arc;

use game_kernel::game_kernel::ecs::*;
use game_kernel::{
    game_kernel::core_systems::tag::TagComponent, physfs_rs::PhysFs, vulkan::VulkanBasic,
    TransformComponent,
};
use game_kernel_ecs::ecs::World;

use game_kernel::rapier3d::prelude::*;
use game_kernel_ecs::*;

use crate::physics;
use crate::utils::{load_gltf, ImportedScene};

use self::follow::FollowComponent;
use self::projectile::ProjectileComponent;

pub mod follow;
pub mod projectile;

pub fn register_components() {
    let mut guard = COMPONENT_FACTORY.lock().unwrap();
    guard.register::<follow::FollowComponent>();
    guard.register::<EnemyComponent>();
    guard.register::<ProjectileComponent>();
}

pub fn init(manager: &mut SystemManager, world: &mut World, fs: &PhysFs, vk: impl VulkanBasic) {
    manager.add_and_init(follow::FollowSystem::new(), world);
    manager.add_and_init(EnemySystem::new(fs, vk), world);
    manager.add_and_init(projectile::ProjectileSystem::new(), world);
}

#[derive(Clone, Copy, Debug)]
pub enum EnemyState {
    Walking(f32),
    Attacking,
    Idling(f32),
}

#[derive(Clone, Copy, Component)]
pub struct EnemyComponent {
    pub health: i32,
    pub melee: u32,
    pub cooldown: f32,
    pub state: EnemyState,
}

impl EnemyComponent {
    pub fn new() -> Self {
        Self {
            health: 100,
            melee: 10,
            cooldown: 1.0,
            state: EnemyState::Walking(2.0),
        }
    }
}

pub struct EnemySystem {
    projectile_mesh: ImportedScene,
    projectile_pfx: Arc<game_kernel::renderer::particle_fx::ParticleSystemBuilderWrapper>,
}

impl EnemySystem {
    pub fn new(fs: &PhysFs, vk: impl VulkanBasic) -> Self {
        Self {
            projectile_mesh: load_gltf("ball-projectile.glb", vk.clone(), fs),
            projectile_pfx: crate::utils::particles::test_particle_builder(fs, vk),
        }
    }
}

impl System for EnemySystem {
    fn init(&mut self, manager: &SystemManager, wolrd: &ecs::World) -> Result<(), SysErr> {
        Ok(())
    }

    fn update(
        &mut self,
        manager: &SystemManager,
        world: &mut ecs::World,
        delta: std::time::Duration,
    ) {
        let player = world.iter_entities().find(|entity| {
            world
                .get_entity_components::<&TagComponent>(*entity)
                .map(|tag| tag.get_name() == "Player entity")
                .unwrap_or(false)
        });
        if player.is_none() {
            return;
        }
        let player = player.unwrap();
        let player_position = world
            .get_entity_components::<&TransformComponent>(player)
            .unwrap()
            .position;

        let mut spawned_projectiles = vec![];

        for (enemy_component, follow_component, transform_component) in world.query::<(
            &mut EnemyComponent,
            &mut FollowComponent,
            &mut TransformComponent,
        )>() {
            enemy_component.state = match enemy_component.state {
                EnemyState::Walking(timer) if timer > 0.0 => {
                    EnemyState::Walking(timer - delta.as_secs_f32())
                }
                EnemyState::Walking(_) => EnemyState::Attacking,
                EnemyState::Attacking => {
                    let dir = cgmath::InnerSpace::normalize(
                        (cgmath::vec3(0.0, 0.5, 0.0) + player_position
                            - transform_component.position),
                    );
                    spawned_projectiles
                        .push((transform_component.position + dir * 2.0, dir * 30.0));
                    EnemyState::Idling(0.5)
                }
                EnemyState::Idling(timer) if timer > 0.0 => {
                    EnemyState::Idling(timer - delta.as_secs_f32())
                }
                EnemyState::Idling(_) => EnemyState::Walking(1.0 + rand::random::<f32>() * 2.0),
            };

            follow_component.speed = match enemy_component.state {
                EnemyState::Walking(_) => 5.0,
                EnemyState::Attacking => 0.0,
                EnemyState::Idling(_) => 0.0,
            };
        }

        for (position, velocity) in spawned_projectiles {
            crate::screen_print!("spawning projectile at {:?}", position);
            projectile::add_projectile(
                world,
                position,
                velocity,
                &self.projectile_mesh,
                self.projectile_pfx.clone(),
            );
        }
    }
}

pub fn add_enemy(world: &mut World, vk: impl VulkanBasic, fs: &PhysFs) {
    let entity = world.add_entity(World::get_root()).unwrap();

    world.add_component(entity, follow::FollowComponent::new(5.0));
    world.add_component(
        entity,
        physics::RigidBodyComponent::from_rigid_body(
            RigidBodyBuilder::new(RigidBodyType::Dynamic)
                .translation(vector![0.0, 10.0, 0.0])
                .lock_rotations()
                .ccd_enabled(true)
                .gravity_scale(6.0)
                .build(),
        ),
    );
    world.add_component(
        entity,
        physics::ColliderComponent::from_collider(
            ColliderBuilder::cuboid(0.3, 2.0, 1.0)
                .user_data(entity as u128)
                .mass(10.0)
                //.active_events(ActiveEvents::CONTACT_EVENTS)
                /*ColliderBuilder::capsule_y(0.5, 0.5)
                .user_data(entity as u128)
                .density(1.1)
                .active_events(ActiveEvents::CONTACT_EVENTS)*/
                .build(),
        ),
    );
    world.add_component(entity, EnemyComponent::new());
    world.add_component(entity, TransformComponent::identity());
    world.add_component(entity, TagComponent::new("Enemy"));

    load_gltf("enemy.glb", vk, fs).load_into_world(world, entity);
}
