use std::any::Any;
use std::collections::HashMap;
use std::sync::Arc;

use cgmath::{vec3, Vector3};

use game_kernel::game_kernel::core_systems::{
    particle_system::ParticleSystemComponent, tag::TagComponent,
};
use game_kernel::game_kernel::ecs::*;
use game_kernel::rapier3d::math::Isometry;
use game_kernel::rapier3d::prelude::*;
use game_kernel::renderer::light::{Light, LightType, PointLight};
use game_kernel::renderer::particle_fx::ParticleSystemBuilderWrapper;
use game_kernel::utils::KeyType;
use game_kernel::{LightComponent, TransformComponent};

use crate::physics::{self, cgna_v3, PhysicsSystem};
use crate::{
    physics::ColliderComponent,
    utils::{load_test_cube, ImportedScene},
};

#[derive(Clone, Copy, Component)]
pub struct ProjectileComponent {
    pub damage: i32,
    pub velocity: Vector3<f32>,
}

impl ProjectileComponent {
    pub fn new(damage: i32, velocity: Vector3<f32>) -> Self {
        Self { damage, velocity }
    }
}

pub struct ProjectileSystem {
    dead_projectiles: Vec<EntitId>,
}

impl ProjectileSystem {
    pub fn new() -> Self {
        Self {
            dead_projectiles: vec![],
        }
    }
}

impl System for ProjectileSystem {
    fn init(&mut self, manager: &SystemManager, wolrd: &ecs::World) -> Result<(), SysErr> {
        Ok(())
    }

    fn update(
        &mut self,
        manager: &SystemManager,
        world: &mut ecs::World,
        delta: std::time::Duration,
    ) {
        let physics_system = manager.get_system_mut::<PhysicsSystem>();
        if physics_system.is_none() {
            return;
        }
        let physics_system = physics_system.unwrap();

        let mut dead_projectiles = vec![];
        for (entity, transform_component, projectile_component) in
            world.query::<(KeyType, &mut TransformComponent, &mut ProjectileComponent)>()
        {
            transform_component.position += projectile_component.velocity * delta.as_secs_f32();
            let (x, y, z) = transform_component.position.into();
            if physics_system
                .query_pipeline
                .intersection_with_shape(
                    &physics_system.rigid_body_set,
                    &physics_system.collider_set,
                    &Isometry::translation(x, y, z),
                    &Ball::new(0.1),
                    QueryFilter::new().predicate(&|_, collider| {
                        collider.user_data as KeyType != entity
                    }),
                )
                //.map(|v| {crate::screen_print!("projectile touched {}", physics_system.collider_set[v].user_data);v})
                .is_some()
            {
                dead_projectiles.push(entity);
            }
        }

        for dead_projectile in self.dead_projectiles.iter() {
            world.rem_entity_recursive(*dead_projectile);
        }

        self.dead_projectiles = dead_projectiles;
    }
}

pub fn add_projectile(
    world: &mut World,
    position: Vector3<f32>,
    velocity: Vector3<f32>,
    mesh: &ImportedScene,
    pfx: Arc<ParticleSystemBuilderWrapper>,
) {
    /*let projectile_sound = rodio::Decoder::new(fs.open_read("audio/Jump.wav").unwrap())
    .unwrap()
    .convert_samples();*/

    let projectile_entity = AddEntity::new(&mut *world, None)
        .unwrap()
        .with_component(ProjectileComponent::new(10, velocity))
        .with_component(TransformComponent::identity().translate(position))
        .with_component(ParticleSystemComponent::new(pfx).with_emitter_position(true))
        .with_component(TagComponent::new("Projectile"))
        .with_component(LightComponent::new(Light {
            light: LightType::Point(PointLight {
                color: vec3(50.0, 60.0, 30.0),
                position: vec3(0.0, 0.0, 0.0),
                size: 1.0,
                influence_radius: 50.0,
            }),
            shadow_map_size: None,
        }))
        .entity;

    world.add_component(
        projectile_entity,
        ColliderComponent::from_collider(
            ColliderBuilder::ball(0.5)
                .user_data(projectile_entity as u128)
                .sensor(true)
                .build(),
        ),
    );
    mesh.load_into_world(world, projectile_entity);
}
