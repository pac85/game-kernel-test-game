use std::any::Any;
use std::collections::HashMap;
use std::sync::Arc;

use cgmath::{num_traits::Float, prelude::*};
use game_kernel::game_kernel::core_systems::tag::TagComponent;
use game_kernel::game_kernel::ecs::*;
use game_kernel::physfs_rs::PhysFs;
use game_kernel::rapier3d::math::Vector;
use game_kernel::vulkan::VulkanBasic;
use game_kernel::TransformComponent;

use crate::physics::{cgna_v3, PhysicsSystem, RigidBodyComponent};

type EntityId = EntitId;

#[derive(Clone, Copy, Component)]
pub struct FollowComponent {
    pub speed: f32,
}

impl FollowComponent {
    pub fn new(speed: f32) -> Self {
        Self { speed }
    }
    pub fn component_new(
        args: HashMap<String, Arc<dyn Any + 'static + Send + Sync>>,
    ) -> Result<Box<dyn Component>, ComponentCreationError> {
        Ok(Box::new(Self {
            speed: parse_component_arg!(args, "speed", f32).as_ref().clone(),
        }))
    }
}

pub struct FollowSystem {
    player_entity: Option<EntityId>,
}

impl FollowSystem {
    pub fn new() -> Self {
        Self {
            player_entity: None,
        }
    }

    pub fn accellerate(
        &self,
        accellerate: f32,
        accel_dir: Vector<f32>,
        velocity: Vector<f32>,
        max_velocity: f32,
        delta_time: f32,
    ) -> Vector<f32> {
        let proj_vel = velocity.dot(&accel_dir);
        let mut accel_vel = accellerate * delta_time;
        if proj_vel + accel_vel > max_velocity {
            accel_vel = max_velocity - proj_vel;
        }

        velocity + (accel_dir * accel_vel.max(0.0))
    }
}

impl System for FollowSystem {
    fn init(&mut self, _manager: &SystemManager, world: &World) -> Result<(), SysErr> {
        Ok(())
    }

    fn update(
        &mut self,
        manager: &SystemManager,
        world: &mut World,
        delta_time: std::time::Duration,
    ) {
        if self.player_entity.is_none() {
            for entity in world.iter_entities() {
                self.player_entity = match world.get_entity_components::<&TagComponent>(entity) {
                    Some(tag) if tag.get_name() == "Player entity" => Some(entity),
                    _ => None,
                };
                if self.player_entity.is_some() {
                    break;
                }
            }
            return;
        }

        let player_entity = self.player_entity.unwrap();

        let player_position = world
            .get_entity_components::<&TransformComponent>(player_entity)
            .unwrap()
            .position;

        let mut physics_system = manager.get_system_mut::<PhysicsSystem>().unwrap();

        for (follow, transform, rigid_body) in world.query::<(
            &mut FollowComponent,
            &mut TransformComponent,
            &mut RigidBodyComponent,
        )>() {
            let dir = (player_position - transform.position)
                .mul_element_wise(cgmath::vec3(1.0, 0.0, 1.0))
                .normalize();
            let dir = cgmath::vec3(dir.x.round(), dir.y.round(), dir.z.round()).normalize();

            //transform.position += dir * follow.speed * (delta_time.as_micros() as f32 / 1000000 as f32);
            let rb_handle = rigid_body.rigid_body.unwrap_handle();
            let pvel = *physics_system.rigid_body_set[rb_handle].linvel();
            let nvel = self.accellerate(
                1000.0,
                cgna_v3(dir),
                pvel,
                follow.speed,
                delta_time.as_secs_f64() as f32,
            );
            physics_system.rigid_body_set[rb_handle].set_linvel(nvel, true);
        }
    }
}
