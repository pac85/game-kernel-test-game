use ::game_kernel::core_systems::base::transform;
use ::game_kernel::core_systems::{base::transform::TransformComponent, light::LightComponent};
use ::game_kernel::ecs::World;
use ::game_kernel::{ecs::COMPONENT_FACTORY, subsystems::video::ui::egui::*};
use cgmath::*;
use egui::{vec2, *};
use game_kernel::{Camera, CameraComponent};

use ::game_kernel::core_systems::{
    relative_transform::RelativeTransformComponent, tag::TagComponent,
};
use game_kernel::input::{EventTypes, Input, KeyTypes, MouseKeys};
use game_kernel::physfs_rs::PhysFs;

use crate::dynamic_match_mut;
use crate::networking::serialization::UuidComponent;

use std::ascii::AsciiExt;
use std::cell::RefCell;
use std::collections::VecDeque;
use std::ops::{Deref, DerefMut};

pub struct OnSreenConsole {
    lines: VecDeque<String>,
    timeout: f32,
}

impl OnSreenConsole {
    pub fn new() -> Self {
        Self {
            lines: VecDeque::new(),
            timeout: 0.0,
        }
    }

    pub fn draw(&mut self, context: &Context, delta: f32) {
        self.timeout -= delta;
        self.timeout = self.timeout.max(0.0);

        if self.timeout == 0.0 {
            self.lines.clear();
        }

        egui::Area::new("log_area")
            .fixed_pos(egui::pos2(30.0, 30.0))
            .show(context, |ui| {
                ui.vertical(|ui| {
                    for line in self.lines.iter() {
                        ui.label(RichText::new(line).size(12.0).color(egui::Color32::WHITE));
                    }
                })
            });
    }

    pub fn print(&mut self, s: impl AsRef<str>) {
        self.lines.push_back(s.as_ref().to_owned());
        if self.lines.len() > 20 {
            self.lines.pop_front();
        }
        self.timeout = 10.0;
    }
}

pub static mut SCREEN_CONSOLE: Option<std::sync::Mutex<OnSreenConsole>> = None;

pub fn screen_print_s(s: impl AsRef<str>) {
    unsafe {
        SCREEN_CONSOLE.as_ref().unwrap().lock().unwrap().print(s);
    }
}

#[macro_export]
macro_rules! screen_print {
    ($($arg:tt)*) => { crate::dbg_ui::screen_print_s(format!($($arg)*)) }
}

pub fn init_screen_console() {
    unsafe {
        SCREEN_CONSOLE = Some(std::sync::Mutex::new(OnSreenConsole::new()));
    }
}
