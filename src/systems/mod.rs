pub mod self_destruct;

use game_kernel::{game_kernel::ecs::COMPONENT_FACTORY, log_msg};
use game_kernel_ecs::ecs::{
    system::{self, *},
    World,
};
use std::{cell::RefCell, sync::Arc};

pub fn register_components() {
    let mut component_factory = COMPONENT_FACTORY.lock().unwrap();

    component_factory.register::<self_destruct::SelfDestructComponent>();
}

pub fn init_systems(manager: &mut SystemManager, world: &mut World) {
    manager
        .add_and_init(self_destruct::SelfDestructSystem::new(), world)
        .unwrap();
}
