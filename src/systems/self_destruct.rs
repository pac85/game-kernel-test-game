use game_kernel::game_kernel::ecs::*;

use std::any::*;
use std::collections::HashMap;
use std::sync::Arc;

type EntityId = EntitId;

#[derive(Debug, Clone, Copy)]
pub enum SelfDestructTimer {
    Frames(u32),
    Seconds(f32),
}

impl SelfDestructTimer {
    pub fn from_frames(frames: u32) -> Self {
        Self::Frames(frames)
    }

    pub fn from_seconds(seconds: f32) -> Self {
        Self::Seconds(seconds)
    }

    fn decrement(&mut self, seconds: f32) -> bool {
        match self {
            Self::Frames(ref mut f) => {
                if *f <= 1 {
                    true
                } else {
                    *f -= 1;
                    false
                }
            }
            Self::Seconds(ref mut s) => {
                if *s <= seconds {
                    true
                } else {
                    *s -= seconds;
                    false
                }
            }
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum KillMode {
    Recursive,
    AppendChildrenTo(EntityId),
}

impl KillMode {
    pub fn recursive() -> Self {
        Self::Recursive
    }

    pub fn append_children_to(new_parent: EntityId) -> Self {
        Self::AppendChildrenTo(new_parent)
    }
}

#[derive(Component)]
pub struct SelfDestructComponent {
    timer: SelfDestructTimer,
    kill_mode: KillMode,
}

impl SelfDestructComponent {
    pub fn new(timer: SelfDestructTimer, kill_mode: KillMode) -> Self {
        Self { timer, kill_mode }
    }

    pub fn component_new(
        args: HashMap<String, Arc<dyn Any + 'static + Send + Sync>>,
    ) -> Result<Box<dyn Component>, ComponentCreationError> {
        Ok(Box::new(Self {
            //path:       parse_component_arg!(args, "path", String).clone(),
            timer: *parse_component_arg!(args, "timer", SelfDestructTimer).clone(),
            kill_mode: *parse_component_arg!(args, "kill_mode", KillMode).clone(),
        }))
    }
}

pub struct SelfDestructSystem {
    to_destroy: Vec<(EntityId, KillMode)>,
}

impl SelfDestructSystem {
    pub fn new() -> Self {
        Self { to_destroy: vec![] }
    }
}

impl System for SelfDestructSystem {
    fn init(&mut self, _manager: &SystemManager, _world: &World) -> Result<(), SysErr> {
        Ok(())
    }

    fn update<'a>(
        &'a mut self,
        _manager: &SystemManager,
        world: &'a mut World,
        delta: std::time::Duration,
    ) {
        self.to_destroy.clear();
        for (entity, component) in world.query::<(EntityId, &mut SelfDestructComponent)>() {
            if component
                .timer
                .decrement(delta.as_micros() as f32 / 1_000_000.0)
            {
                self.to_destroy.push((entity, component.kill_mode));
            }
        }

        for (entity, mode) in self.to_destroy.drain(0..self.to_destroy.len()) {
            let _e = match mode {
                KillMode::Recursive => world.rem_entity_recursive(entity),
                KillMode::AppendChildrenTo(new_parent) => world.rem_entity(entity, new_parent),
            };
        }
    }
}
