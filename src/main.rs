use game_kernel_test_game::{generic_main, MainArgs};

fn main() {
    generic_main(MainArgs::Local, 0)
}
