use game_kernel_test_game::generic_main;
use game_kernel_test_game::networking::netsys::ServerSystem;
use game_kernel_test_game::networking::*;
use std::env;
use std::thread;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        println!("expected address");
        panic!("");
    }

    if args.len() < 3 {
        println!("expected udp address");
        panic!("");
    }
    let (mut server, rx) = Server::new(&args[1], &args[2]).unwrap();
    let udp_socket = server.clone_udp_socket().unwrap();
    thread::spawn(move || {
        server.server_loop();
    });
    generic_main(
        game_kernel_test_game::MainArgs::Server(ServerSystem::new(rx, udp_socket)),
        0,
    )
}
