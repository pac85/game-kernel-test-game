use game_kernel_test_game::networking::netsys::ClientSystem;
use game_kernel_test_game::{generic_main, networking::*};
use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        println!("expected address");
        panic!("");
    }
    let client = Client::new(&args[1]).unwrap();
    let seed = client.seed();
    let client_system = ClientSystem::new(client);
    generic_main(game_kernel_test_game::MainArgs::Client(client_system), seed);
}
