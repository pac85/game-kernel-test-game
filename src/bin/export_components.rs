use game_kernel_test_game::*;
use serde_json::value::Value;

pub fn main() {
    systems::register_components();
    player::pickup::register();
    {
        let mut components = game_kernel::game_kernel::ecs::COMPONENT_FACTORY
            .lock()
            .unwrap();

        physics::register::register_components(&mut components);
    }
    enemy::register_components();
    game_kernel::game_kernel::core_systems::register_components();

    let components = game_kernel::game_kernel::ecs::COMPONENT_FACTORY
        .lock()
        .unwrap();
    let mut components_values = vec![];
    components.all_names().for_each(|component_name| {
        let fields = components.get_component_fields(&component_name);
        components_values.push(Value::Object(serde_json::Map::from_iter([(
            component_name.clone(),
            Value::Object(serde_json::Map::from_iter(fields.unwrap().iter().map(
                |(name, field)| (name.clone(), Value::String(field.type_name.to_owned())),
            ))),
        )])));
    });

    let components_values = Value::Array(components_values);

    println!("{}", components_values.to_string());
}
