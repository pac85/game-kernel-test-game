use std::{io::Cursor, net::UdpSocket, sync::mpsc::Receiver};

use byteorder::{LittleEndian, ReadBytesExt};
use game_kernel::{
    ecs::*,
    input::{Input, Since},
};

use crate::{networking::serialization::UuidGenrator, player::Player};

use super::{
    serialization::{component_assign, ClientUpdate, EntitySnapshot, Snapshot, UuidComponent},
    Client, ClientConnection,
};

pub fn register_components() {
    let mut lock = COMPONENT_FACTORY.lock().unwrap();

    lock.register::<NetSyncComponent>();
    lock.register::<UuidComponent>();
}

#[derive(Clone)]
pub enum NetSyncMode {
    SIMPLE,
    ClientAuthoritative,
}

#[derive(Clone, MetaStruct, Component)]
pub struct NetSyncComponent {
    pub mode: NetSyncMode,
}

impl NetSyncComponent {
    pub fn client_authoritative() -> Self {
        Self {
            mode: NetSyncMode::ClientAuthoritative,
        }
    }
}

impl Default for NetSyncComponent {
    fn default() -> Self {
        Self {
            mode: NetSyncMode::SIMPLE,
        }
    }
}

struct ServerPlayer {
    pub input: Input,
    pub player: Player,
}

impl ServerPlayer {
    pub fn new(input: Input, player: Player) -> Self {
        Self { input, player }
    }
}

pub struct ServerSystem {
    rx: Receiver<ClientConnection>,
    clients: Vec<(ClientConnection, ServerPlayer)>,
    udp_socket: UdpSocket,
}

impl ServerSystem {
    pub fn new(rx: Receiver<ClientConnection>, udp_socket: UdpSocket) -> Self {
        Self {
            rx,
            clients: vec![],
            udp_socket,
        }
    }
}

impl System for ServerSystem {
    fn init(
        &mut self,
        manager: &::game_kernel::ecs::SystemManager,
        wolrd: &game_kernel_ecs::ecs::World,
    ) -> Result<(), ::game_kernel::ecs::SysErr> {
        Ok(())
    }

    fn update(
        &mut self,
        manager: &::game_kernel::ecs::SystemManager,
        world: &mut game_kernel_ecs::ecs::World,
        delta: std::time::Duration,
    ) {
        use ::game_kernel::core_systems::camera_system::CameraSystem;
        use ::game_kernel::core_systems::physics::PhysicsSystem;
        use ::game_kernel::core_systems::renderer::RendererSystem;
        use ::game_kernel::core_systems::sound_system::SoundSystem;

        let renderer = manager.get_system::<RendererSystem>().unwrap();
        let sound = manager.get_system::<SoundSystem>().unwrap();
        let main_input = manager.get_resource::<Input>();
        {
            let fs = manager.get_resource::<game_kernel::physfs_rs::PhysFs>();

            while let Ok(mut client) = self.rx.try_recv() {
                let seed = {
                    let data = client.recv_reliable_message().unwrap();
                    let mut cursor = Cursor::new(data);
                    cursor.read_u64::<LittleEndian>().unwrap()
                };
                let camera_system = manager.get_system_mut::<CameraSystem>().unwrap();
                let player = Player::new(
                    renderer.get_renderer().borrow().get_vulkan_common(),
                    world,
                    camera_system,
                    &*fs,
                    &mut *sound.get_ctx().borrow().renderer.0.lock().unwrap(),
                    &mut UuidGenrator::new(seed),
                    false,
                );
                self.clients
                    .push((client, ServerPlayer::new(main_input.clone_config(), player)));
            }
        }

        let mut snapshot = Snapshot::new();
        for (entity_id, uuid_component, ns) in
            world.query::<(EntitId, &UuidComponent, &NetSyncComponent)>()
        {
            let mut entity_snapshot = EntitySnapshot::new(uuid_component.uuid());
            for component in world.get_all_entity_components(entity_id) {
                entity_snapshot.add_component(Component::dyn_clone(&*component));
            }
            snapshot.add_entity(entity_snapshot);
        }

        let serialized = snapshot.serialize();

        if serialized.len() == 0 {
            return;
        }
        for (client, player) in self.clients.iter_mut() {
            //receive input
            {
                let r = client
                    .recv_unreliable_message(&mut self.udp_socket)
                    .unwrap();
                let update = ClientUpdate::deserialize(r);
                match update {
                    ClientUpdate::InputEvents(events) => {
                        for (event, timestamp) in events {
                            match event {
                                game_kernel::input::EventTypes::Empty => (),
                                _ => println!("recevied {event:?}"),
                            }
                            player.input.register_event(event, timestamp);
                        }
                    }
                    _ => (),
                }
            }
            player.input.on_frame_start();
            {
                let physics = manager.get_system_mut::<PhysicsSystem>().unwrap();
                player.player.update(
                    None,
                    world,
                    &player.input,
                    physics,
                    &mut *sound.get_ctx().borrow().renderer.0.lock().unwrap(),
                    (delta.as_secs_f64()) as f32,
                );
            }
            client
                .send_unreliable_message(&mut self.udp_socket, &serialized)
                .unwrap();

            //receive states
            {
                let r = client
                    .recv_unreliable_message(&mut self.udp_socket)
                    .unwrap();
                let update = ClientUpdate::deserialize(r);
                match update {
                    ClientUpdate::ClientSnapshot(data) => {
                        let snapshot = Snapshot::deserialize(data).unwrap();

                        for (entity_id, uuid_component, ns) in
                            world.query::<(EntitId, &UuidComponent, &NetSyncComponent)>()
                        {
                            match ns.mode {
                                NetSyncMode::ClientAuthoritative => {
                                    if let Some(entity_snapshot) =
                                        snapshot.get(&uuid_component.uuid())
                                    {
                                        for component in entity_snapshot.components() {
                                            let target_component = world
                                                .get_entity_component_dyn_mut(
                                                    component.type_id(),
                                                    entity_id,
                                                );
                                            if target_component.is_none() {
                                                continue;
                                            }
                                            let target_component = target_component.unwrap();
                                            component_assign(target_component, component);
                                        }
                                    }
                                }
                                NetSyncMode::SIMPLE => {}
                            }
                        }
                    }
                    _ => (),
                }
            }

            player.input.on_frame_end();
        }
    }
}

pub struct ClientSystem {
    client: Client,
    input_since: Since,
}

impl ClientSystem {
    pub fn new(client: Client) -> Self {
        Self {
            client,
            input_since: Since::Ever,
        }
    }
}

impl System for ClientSystem {
    fn init(&mut self, manager: &SystemManager, wolrd: &ecs::World) -> Result<(), SysErr> {
        Ok(())
    }

    fn update(
        &mut self,
        manager: &SystemManager,
        world: &mut ecs::World,
        delta: std::time::Duration,
    ) {
        //send input
        {
            let input = manager.get_resource::<Input>();
            let events = input.events_since(self.input_since);
            self.input_since = events
                .last()
                .map(|(_, t)| Since::Timestamp(*t))
                .unwrap_or(self.input_since);
            let update = ClientUpdate::InputEvents(events);
            self.client
                .send_unreliable_message(update.serialize())
                .unwrap();
        }
        let data = self.client.recv_unreliable_message().unwrap();
        let snapshot = Snapshot::deserialize(data).unwrap();

        let mut client_snapshot = Snapshot::new();
        for (entity_id, uuid_component, ns) in
            world.query::<(EntitId, &UuidComponent, &NetSyncComponent)>()
        {
            match ns.mode {
                NetSyncMode::SIMPLE => {
                    if let Some(entity_snapshot) = snapshot.get(&uuid_component.uuid()) {
                        for component in entity_snapshot.components() {
                            let target_component =
                                world.get_entity_component_dyn_mut(component.type_id(), entity_id);
                            if target_component.is_none() {
                                continue;
                            }
                            let target_component = target_component.unwrap();
                            component_assign(target_component, component);
                        }
                    }
                }
                NetSyncMode::ClientAuthoritative => {
                    let mut entity_snapshot = EntitySnapshot::new(uuid_component.uuid());
                    for component in world.get_all_entity_components(entity_id) {
                        entity_snapshot.add_component(Component::dyn_clone(&*component));
                    }
                    client_snapshot.add_entity(entity_snapshot);
                }
            }
        }

        let serialized = client_snapshot.serialize();
        if serialized.len() != 0 {
            self.client
                .send_unreliable_message(ClientUpdate::ClientSnapshot(serialized).serialize())
                .unwrap();
        }
    }
}
