pub trait Message {
    fn type_hash(&self) -> u64;
    fn len(&self) -> usize;
    fn data(&self) -> Vec<u8>;
}

pub struct GenericMessage {
    type_hash: u64,
    len: usize,
    data: Vec<u8>,
}

impl Message for GenericMessage {
    fn type_hash(&self) -> u64 {
        self.type_hash
    }

    fn len(&self) -> usize {
        self.len
    }

    fn data(&self) -> Vec<u8> {
        self.data.clone()
    }
}
