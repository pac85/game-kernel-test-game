pub mod message;
pub mod netsys;
pub mod serialization;

use std::{
    io::{Cursor, Read, Seek, Write},
    mem::size_of,
    net::{TcpListener, TcpStream, UdpSocket},
    sync::mpsc::{channel, Receiver, Sender},
};

use byteorder::{ByteOrder, LittleEndian, ReadBytesExt, WriteBytesExt};

pub struct Server {
    listener: TcpListener,
    udp_addr: String,
    udp_socket: UdpSocket,
    sender: Sender<ClientConnection>,
}

impl Server {
    pub fn new(
        tcp_addr: impl AsRef<str>,
        udp_addr: impl AsRef<str>,
    ) -> std::io::Result<(Self, Receiver<ClientConnection>)> {
        let (sender, receiver) = channel();
        Ok((
            Self {
                listener: TcpListener::bind(tcp_addr.as_ref())?,
                udp_addr: udp_addr.as_ref().to_owned(),
                udp_socket: UdpSocket::bind(udp_addr.as_ref().to_owned())?,
                sender,
            },
            receiver,
        ))
    }

    pub fn clone_udp_socket(&self) -> std::io::Result<UdpSocket> {
        self.udp_socket.try_clone()
    }

    pub fn accept(&self, mut incoming: TcpStream) -> std::io::Result<ClientConnection> {
        println!(
            "Accepting connection from {:?}",
            incoming.peer_addr().unwrap()
        );
        let data = recv_reliable_message(&mut incoming)?;
        assert_eq!(String::from_utf8_lossy(&data), "gktgc");
        send_reliable_message(&mut incoming, "gktgs")?;
        //sending udp address
        send_reliable_message(&mut incoming, self.udp_addr.as_bytes())?;
        let mut buf = [0u8; 20];
        match self.udp_socket.recv_from(&mut buf) {
            Ok((len, client_udp_addr)) => {
                if String::from_utf8_lossy(&buf[..len]) != "gktgc" {
                    return Err(std::io::Error::new(
                        std::io::ErrorKind::Other,
                        "invalid request",
                    ));
                }
                println!("Client udp address {client_udp_addr}");
                //let client_udp_socket = UdpSocket::bind(client_udp_addr)?;
                self.udp_socket
                    .send_to("gktgs".as_bytes(), client_udp_addr)?;
                Ok(ClientConnection {
                    tcp_stream: incoming,
                    udp_address: client_udp_addr.to_string(),
                })
            }
            Err(e) => Err(e),
        }
    }

    pub fn server_loop(&mut self) {
        for incoming in self.listener.incoming() {
            if incoming.is_err() {
                println!("{incoming:?}");
                continue;
            }

            match self.accept(incoming.unwrap()) {
                Ok(client_connection) => {
                    self.sender.send(client_connection).unwrap();
                }
                Err(e) => {
                    println!("{e}");
                    continue;
                }
            }
        }
    }
}

pub fn send_reliable_message(
    tcp_stream: &mut TcpStream,
    data: impl AsRef<[u8]>,
) -> std::io::Result<()> {
    tcp_stream.write_u64::<LittleEndian>(data.as_ref().len() as u64)?;
    tcp_stream.write(data.as_ref())?;

    println!("sent {} {:?}", data.as_ref().len(), data.as_ref());

    Ok(())
}

pub fn recv_reliable_message(tcp_stream: &mut TcpStream) -> std::io::Result<Vec<u8>> {
    let len = tcp_stream.read_u64::<LittleEndian>()? as usize;
    let mut v: Vec<u8> = Vec::with_capacity(len);
    v.resize(len, 0);
    tcp_stream.read_exact(&mut v)?;

    Ok(v)
}

pub fn send_unreliable_message(
    udb_socket: &mut UdpSocket,
    addr: impl AsRef<str>,
    data: impl AsRef<[u8]>,
) -> std::io::Result<()> {
    let mut cursor = Cursor::new(Vec::<u8>::new());
    //cursor.write_u64::<LittleEndian>(data.as_ref().len() as _)?;
    cursor.write_all(data.as_ref())?;
    udb_socket.send_to(&cursor.into_inner(), addr.as_ref())?;
    Ok(())
}

pub fn recv_unreliable_message(udp_socket: &mut UdpSocket) -> std::io::Result<Vec<u8>> {
    let mut buf = Vec::with_capacity(2000);
    buf.resize(2000, 0);
    let len = udp_socket.recv(&mut buf)?;
    buf.resize(len, 0);

    Ok(buf)
}

pub struct ClientConnection {
    pub tcp_stream: TcpStream,
    pub udp_address: String,
}

impl ClientConnection {
    pub fn send_reliable_message(&mut self, data: impl AsRef<[u8]>) -> std::io::Result<()> {
        send_reliable_message(&mut self.tcp_stream, data)
    }

    pub fn recv_reliable_message(&mut self) -> std::io::Result<Vec<u8>> {
        recv_reliable_message(&mut self.tcp_stream)
    }

    pub fn send_unreliable_message(
        &mut self,
        udp_socket: &mut UdpSocket,
        data: impl AsRef<[u8]>,
    ) -> std::io::Result<()> {
        send_unreliable_message(udp_socket, &self.udp_address, data)
    }

    pub fn recv_unreliable_message(
        &mut self,
        udp_socket: &mut UdpSocket,
    ) -> std::io::Result<Vec<u8>> {
        recv_unreliable_message(udp_socket)
    }
}

pub struct Client {
    client_connection: ClientConnection,
    udp_socket: UdpSocket,
    seed: u64,
}

impl Client {
    pub fn new(addr: impl AsRef<str>) -> std::io::Result<Self> {
        let mut tcp_stream = TcpStream::connect(addr.as_ref())?;
        send_reliable_message(&mut tcp_stream, "gktgc".as_bytes())?;

        let response = recv_reliable_message(&mut tcp_stream)?;
        let response = String::from_utf8_lossy(&response);
        if &response != "gktgs" {
            return Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                "invalid response",
            ));
        }
        println!("Connected to {}", addr.as_ref());
        //receive udp address
        let udp_address = recv_reliable_message(&mut tcp_stream)?;
        let udp_address = String::from_utf8_lossy(&udp_address);
        println!("udp server address: {}", udp_address);
        let mut udp_socket = UdpSocket::bind("127.0.0.1:1222")?;
        udp_socket.send_to("gktgc".as_bytes(), &*udp_address)?;

        let response = recv_unreliable_message(&mut udp_socket)?;
        let response = String::from_utf8_lossy(&response);
        if &response != "gktgs" {
            return Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                "invalid response",
            ));
        }
        let seed: u64 = rand::random();
        {
            let mut cursor = Cursor::new(vec![]);
            cursor.write_u64::<LittleEndian>(seed).unwrap();
            send_reliable_message(&mut tcp_stream, cursor.into_inner())?;
        }

        Ok(Self {
            client_connection: ClientConnection {
                tcp_stream,
                udp_address: udp_address.to_string(),
            },
            udp_socket,
            seed,
        })
    }

    pub fn recv_unreliable_message(&mut self) -> std::io::Result<Vec<u8>> {
        self.client_connection
            .recv_unreliable_message(&mut self.udp_socket)
    }

    pub fn send_unreliable_message(&mut self, data: impl AsRef<[u8]>) -> std::io::Result<()> {
        self.client_connection
            .send_unreliable_message(&mut self.udp_socket, data)
    }

    pub fn seed(&self) -> u64 {
        self.seed
    }
}
