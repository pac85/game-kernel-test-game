use std::{
    any::{Any, TypeId},
    collections::HashMap,
    io::{Cursor, Read, Seek, Write},
    sync::Arc,
};

use byteorder::{LittleEndian, ReadBytesExt, WriteBytesExt};
use cgmath::{prelude, vec3, Quaternion, Vector3};
use game_kernel::{ecs::*, input::EventTypes};
use serde::{Deserialize, Serialize};

use super::message::GenericMessage;

use crate::{dynamic_match, dynamic_match_mut};

pub use game_kernel::core_systems::uuid::*;

pub trait WriteSeek: Write + Seek {}
impl<W: Write + Seek> WriteSeek for W {}

pub trait ReadSeek: Read + Seek {
    fn as_read_dyn(&self) -> &dyn Read;
    fn as_read_dyn_mut(&mut self) -> &mut dyn Read;
}
impl<R: Read + Seek> ReadSeek for R {
    fn as_read_dyn(&self) -> &dyn Read {
        self as _
    }

    fn as_read_dyn_mut(&mut self) -> &mut dyn Read {
        self as _
    }
}

#[rustfmt::skip::macros(dynamic_match)]
fn serialize_component(w: &mut dyn WriteSeek, c: impl AsRef<dyn Component>) -> std::io::Result<()> {
    let type_id = c.as_ref().gk_as_any().type_id();
    let type_id: u64 = unsafe { std::mem::transmute(type_id) };
    //write type
    w.write_u64::<LittleEndian>(type_id)?;
    for field in c.as_ref().get_public_fields() {
        let field_any = c.as_ref().get_field_any(field).unwrap();
        dynamic_match!(field_any,
            (u8)(u) => { w.write(&[*u])?; },
            (u16)(u) => { w.write_u16::<LittleEndian>(*u)?; },
            (u32)(u) => { w.write_u32::<LittleEndian>(*u)?; },
            (u64)(u) => { w.write_u64::<LittleEndian>(*u)?; },
            (i8)(i) => { w.write(&[*i as u8])?; },
            (i16)(i) => { w.write_i16::<LittleEndian>(*i)?; },
            (i32)(i) => { w.write_i32::<LittleEndian>(*i)?; },
            (i64)(i) => { w.write_i64::<LittleEndian>(*i)?; },
            (f32)(f) => { w.write_f32::<LittleEndian>(*f)?; },
            (f64)(f) => { w.write_f64::<LittleEndian>(*f)?; },
            (bool)(b) => { w.write(&[if *b {1} else {0}])?; },

            (Vector3<f32>)(v) => {
                w.write_f32::<LittleEndian>(v.x)?;
                w.write_f32::<LittleEndian>(v.y)?;
                w.write_f32::<LittleEndian>(v.z)?;
            },
            (Quaternion<f32>)(q) => {
                w.write_f32::<LittleEndian>(q.s)?;
                w.write_f32::<LittleEndian>(q.v.x)?;
                w.write_f32::<LittleEndian>(q.v.y)?;
                w.write_f32::<LittleEndian>(q.v.z)?;
            },

            _ => {}
        );
    }

    Ok(())
}

macro_rules! type_match {
    ($type:expr, ($f_t:ty) => $f_b:block $(, ($t:ty) => $b:block)*, _ => $d_b:block) => {
        if $type == TypeId::of::<$f_t>() {
            $f_b
        }
        $(
            else if $type == TypeId::of::<$t>() {
                $b
            }
        )*
        else {
            $d_b
        }
    }
}

#[rustfmt::skip::macros(dynamic_match)]
fn deserialize_component(r: &mut dyn Read) -> std::io::Result<Box<dyn Component>> {
    let type_id = r.read_u64::<LittleEndian>()?;
    let type_id: TypeId = unsafe { std::mem::transmute(type_id) };
    let mut lock = META_STRUCT_FACTORY.lock().unwrap();
    let component_type_name = lock.get_name(type_id).unwrap().clone();
    let fields_map = lock.get_fields(&component_type_name).unwrap();
    fn wrap_arg<T: Any + Send + Sync + 'static>(c: T) -> Arc<dyn Any + Send + Sync + 'static> {
        Arc::new(c)
    }

    let mut arg_map = HashMap::new();
    let mut arg_fields_vec: Vec<_> = fields_map.iter().collect();
    arg_fields_vec.sort_by_key(|(_, f)| f.ordering);
    for (field_name, field_info) in arg_fields_vec.iter().filter(|(_, f)| f.is_public) {
        let arg = type_match!(field_info.type_id,
            (u8) => {
                let mut buf = [0; 1];
                r.read_exact(&mut buf)?;
                wrap_arg(buf[0])
            },
            (u16) => { wrap_arg(r.read_u16::<LittleEndian>()?) },
            (u32) => { wrap_arg(r.read_u32::<LittleEndian>()?) },
            (u64) => { wrap_arg(r.read_u64::<LittleEndian>()?) },
            (i8) => {
                let mut buf = [0; 1];
                r.read_exact(&mut buf)?;
                wrap_arg(buf[0] as i8)
            },
            (i16) => { wrap_arg(r.read_i16::<LittleEndian>()?) },
            (i32) => { wrap_arg(r.read_i32::<LittleEndian>()?) },
            (i64) => { wrap_arg(r.read_i64::<LittleEndian>()?) },
            (f32) => { wrap_arg(r.read_f32::<LittleEndian>()?) },
            (f64) => { wrap_arg(r.read_f64::<LittleEndian>()?) },
            (bool) => {
                let mut buf = [0; 1];
                r.read_exact(&mut buf)?;
                wrap_arg(buf[0] != 0)
            },

            (Vector3<f32>) => { wrap_arg(vec3(
                r.read_f32::<LittleEndian>()?,
                r.read_f32::<LittleEndian>()?,
                r.read_f32::<LittleEndian>()?,
            )) },

            (Quaternion<f32>) => { wrap_arg(Quaternion::new(
                r.read_f32::<LittleEndian>()?,
                r.read_f32::<LittleEndian>()?,
                r.read_f32::<LittleEndian>()?,
                r.read_f32::<LittleEndian>()?,
            )) },

            _ => { continue; }
        );

        arg_map.insert((*field_name).clone(), arg);
    }

    drop(lock);
    let mut lock = COMPONENT_FACTORY.lock().unwrap();
    lock.instantiate(&component_type_name, arg_map)
        .map_err(|err| std::io::Error::new(std::io::ErrorKind::Other, format!("{err}")))
}

pub fn component_assign(
    mut a: impl std::ops::DerefMut<Target = dyn Component>,
    b: &Box<dyn Component>,
) {
    let a = a.deref_mut();
    if a.gk_as_any().type_id() != b.gk_as_any().type_id() {
        return;
    }

    for field in a.get_fields() {
        dynamic_match_mut!(a.get_field_any_mut(field).unwrap(),
            (u8)(u) => { *u = *b.get_field(field).unwrap(); },
            (u16)(u) => { *u = *b.get_field(field).unwrap(); },
            (u32)(u) => { *u = *b.get_field(field).unwrap(); },
            (u64)(u) => { *u = *b.get_field(field).unwrap(); },
            (i8)(i) => { *i = *b.get_field(field).unwrap(); },
            (i16)(i) => { *i = *b.get_field(field).unwrap(); },
            (i32)(i) => { *i = *b.get_field(field).unwrap(); },
            (i64)(i) => { *i = *b.get_field(field).unwrap(); },
            (f32)(f) => { *f = *b.get_field(field).unwrap(); },
            (f64)(f) => { *f = *b.get_field(field).unwrap(); },
            (bool)(v) => { *v = *b.get_field(field).unwrap(); },

            (Vector3<f32>)(v) => {
                *v = *b.get_field(field).unwrap();
            },
            (Quaternion<f32>)(q) => {
                *q = *b.get_field(field).unwrap();
            },

            _ => {}
        );
    }
}

#[derive(Clone)]
pub struct EntitySnapshot {
    entity_uuid: u64,
    components: Vec<Box<dyn Component>>,
}

impl EntitySnapshot {
    pub fn new(entity_uuid: u64) -> Self {
        Self {
            entity_uuid,
            components: vec![],
        }
    }

    pub fn add_component(&mut self, c: Box<dyn Component>) {
        self.components.push(c);
    }

    pub fn serialize(&self, w: &mut dyn WriteSeek) -> std::io::Result<()> {
        w.write_u64::<LittleEndian>(self.entity_uuid)?;
        w.write_u64::<LittleEndian>(self.components.len() as _);

        for component in self.components.iter() {
            let start = w.stream_position()?;
            w.write_u64::<LittleEndian>(0)?;
            serialize_component(w, component)?;
            let end = w.stream_position()?;
            w.seek(std::io::SeekFrom::Start(start))?;
            w.write_u64::<LittleEndian>(end)?;
            w.seek(std::io::SeekFrom::Start(end))?;
        }

        Ok(())
    }

    pub fn deserialize(r: &mut dyn ReadSeek) -> std::io::Result<Self> {
        let entity_uuid = r.read_u64::<LittleEndian>()?;
        let components_len = r.read_u64::<LittleEndian>()? as usize;
        let mut components = Vec::with_capacity(components_len);

        for _ in 0..components_len {
            let component_end = r.read_u64::<LittleEndian>()?;
            if let Ok(component) = deserialize_component(r.as_read_dyn_mut()) {
                println!(
                    "success deserializing component {}",
                    component.get_name_dyn()
                );
                components.push(component);
            } else {
                println!("failed to deserialize component, seeking to {component_end}");
                r.seek(std::io::SeekFrom::Start(component_end))?;
            }
            //components.push(deserialize_component(r.as_read_dyn_mut())?);
        }

        Ok(Self {
            entity_uuid,
            components,
        })
    }

    pub fn components(&self) -> impl Iterator<Item = &Box<dyn Component>> {
        self.components.iter()
    }
}

#[derive(Serialize, Deserialize)]
pub enum ClientUpdate {
    InputEvents(Vec<(EventTypes, f64)>),
    ClientSnapshot(Vec<u8>),
}

impl ClientUpdate {
    pub fn serialize(&self) -> Vec<u8> {
        let mut cursor = Cursor::new(vec![]);
        ciborium::ser::into_writer(self, &mut cursor).unwrap();
        cursor.into_inner()
    }

    pub fn deserialize(data: Vec<u8>) -> Self {
        let cursor = Cursor::new(data);
        ciborium::de::from_reader(cursor).unwrap()
    }
}

pub struct Snapshot {
    entities: Vec<EntitySnapshot>,
}

impl Snapshot {
    pub fn new() -> Self {
        Self { entities: vec![] }
    }

    pub fn add_entity(&mut self, e: EntitySnapshot) {
        self.entities.push(e);
    }

    pub fn serialize(&self) -> Vec<u8> {
        let mut cursor = Cursor::new(Vec::<u8>::new());
        for entity in self.entities.iter() {
            entity.serialize(&mut cursor).unwrap();
        }
        let res = cursor.into_inner();
        res
    }

    pub fn deserialize(data: impl AsRef<[u8]>) -> std::io::Result<HashMap<u64, EntitySnapshot>> {
        let mut map = HashMap::new();
        let mut cursor = Cursor::new(data.as_ref());
        while cursor.position() < data.as_ref().len() as _ {
            let entity_snapshot = EntitySnapshot::deserialize(&mut cursor)?;
            map.insert(entity_snapshot.entity_uuid, entity_snapshot);
        }
        Ok(map)
    }
}

pub mod test {
    use std::io::{Cursor, Seek};

    use game_kernel::TransformComponent;

    use super::*;

    #[derive(Clone, Copy, Debug, PartialEq, MetaStruct, Component)]
    pub struct TestC {
        pub i: i32,
        pub u: u32,
        pub v: Vector3<f32>,
        pub q: Quaternion<f32>,
    }

    #[derive(Clone, Debug, PartialEq, MetaStruct, Component)]
    pub struct UnserC {
        hmap: HashMap<String, u32>,
    }

    impl UnserC {
        pub fn new() -> Self {
            Self {
                hmap: HashMap::new(),
            }
        }
    }

    #[test]
    fn test_ser_de() {
        {
            let mut lock = COMPONENT_FACTORY.lock().unwrap();
            lock.register::<TestC>();
        }

        let mut cursor = Cursor::new(Vec::<u8>::new());

        let testi = TestC {
            i: 1,
            u: 2,
            v: vec3(1.0, 2.0, 3.0),
            q: Quaternion::new(1.0, 2.0, 2.0, 3.0),
        };
        serialize_component(&mut cursor, Box::new(testi.clone()) as Box<dyn Component>).unwrap();

        cursor.seek(std::io::SeekFrom::Start(0)).unwrap();
        let desered = deserialize_component(&mut cursor).unwrap();
        let desered = desered.gk_as_any().downcast_ref::<TestC>().unwrap();
        assert_eq!(&testi, desered);
    }

    #[test]
    fn test_e() {
        {
            let mut lock = COMPONENT_FACTORY.lock().unwrap();
            lock.register::<TestC>();
            lock.register::<TransformComponent>();
            lock.register::<UuidComponent>();
            lock.register::<UnserC>();
        }

        let mut e = EntitySnapshot::new(1);
        let testi = TestC {
            i: 1,
            u: 2,
            v: vec3(1.0, 2.0, 3.0),
            q: Quaternion::new(1.0, 2.0, 2.0, 3.0),
        };
        e.add_component(Box::new(UuidComponent::new(1)));
        e.add_component(Box::new(UnserC::new())); //wont ve serialized correctly
        e.add_component(Box::new(testi));
        e.add_component(Box::new(TransformComponent::identity()));

        let mut snapshot = Snapshot::new();
        snapshot.add_entity(e);

        let mut e = EntitySnapshot::new(1);
        e.add_component(Box::new(TransformComponent::identity()));
        snapshot.add_entity(e);

        let serialized = snapshot.serialize();
        let desered = Snapshot::deserialize(serialized);
        //assert_eq!(desered.entity_uuid, e.entity_uuid);
        for (entity, components) in desered.unwrap().iter() {
            for component in components.components() {
                dynamic_match!(component.gk_as_any(),
                    (TransformComponent)(t) => { assert_eq!(t, &TransformComponent::identity()) },
                    (TestC)(t) => { assert_eq!(t, &testi) },
                    _ => { panic!("wtf"); }
                );
            }
        }
    }
}
