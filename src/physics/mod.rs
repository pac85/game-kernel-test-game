use core::panic;
use std::any::Any;
use std::collections::{HashMap, HashSet};
use std::sync::{Arc, RwLock};
use std::{cell::RefCell, rc::Rc};

use cgmath::{Deg, Quaternion, Rotation3};
use game_kernel::game_kernel::core_systems::tag::TagComponent;
use game_kernel::game_kernel::ecs::*;
use game_kernel::game_kernel::subsystems::Subsystems;
use game_kernel::utils::{self, HashFramestamps};
use game_kernel::TransformComponent;

pub use game_kernel::game_kernel::core_systems::physics::*;

use game_kernel::rapier3d::prelude::*;

pub fn test(sbsts: &mut Subsystems) {
    AddEntity::new(&mut *sbsts.world.borrow_mut(), None)
        .unwrap()
        .with_component(ColliderComponent::from_collider(
            ColliderBuilder::cuboid(100.0, 0.1, 100.0)
                .rotation(vector![0.0, 0.0, 0.0])
                .build(),
        ))
        .with_component(TransformComponent::identity().translate((0.0, -10.0, 0.0).into()))
        .with_component(TagComponent::new("Floor"));

    AddEntity::new(&mut *sbsts.world.borrow_mut(), None)
        .unwrap()
        .with_component(ColliderComponent::from_collider(
            ColliderBuilder::cuboid(100.0, 100.0, 0.1)
                .rotation(vector![0.0, 0.0, 0.0])
                .build(),
        ))
        .with_component(TransformComponent::identity().translate((0.0, 0.0, 12.0).into()))
        .with_component(TagComponent::new("wall"));

    AddEntity::new(&mut *sbsts.world.borrow_mut(), None)
        .unwrap()
        .with_component(ColliderComponent::from_collider(
            ColliderBuilder::cuboid(100.0, 100.0, 0.1)
                .rotation(vector![0.0, 0.0, 0.0])
                .build(),
        ))
        .with_component(TransformComponent::identity().translate((0.0, 0.0, -12.0).into()))
        .with_component(TagComponent::new("wall"));

    let (cyl_mesh, cyl_trans) = crate::utils::load_gltf_mesh(
        "tube.glb",
        sbsts.get_vulkan_common().unwrap(),
        &sbsts.physfs,
        "Cylinder",
    )
    .unwrap();
    AddEntity::new(&mut *sbsts.world.borrow_mut(), None)
        .unwrap()
        .with_component(ColliderComponent::from_collider(
            ColliderBuilder::trimesh(
                cyl_mesh
                    .vertex_buffer
                    .iter()
                    .map(|v| point![v.x, v.y, v.z])
                    .collect(),
                cyl_mesh
                    .index_buffer
                    .chunks_exact(3)
                    .map(|s| [s[0], s[1], s[2]])
                    .collect(),
            )
            .user_data(69420)
            .build(),
        ))
        .with_component(cyl_trans)
        .with_component(TagComponent::new("tube"));

    let (cyl_mesh, cyl_trans) = crate::utils::load_gltf_mesh(
        "tube.glb",
        sbsts.get_vulkan_common().unwrap(),
        &sbsts.physfs,
        "Cube",
    )
    .unwrap();
    AddEntity::new(&mut *sbsts.world.borrow_mut(), None)
        .unwrap()
        .with_component(ColliderComponent::from_collider(
            ColliderBuilder::trimesh(
                cyl_mesh
                    .vertex_buffer
                    .iter()
                    .map(|v| point![v.x, v.y, v.z])
                    .collect(),
                cyl_mesh
                    .index_buffer
                    .chunks_exact(3)
                    .map(|s| [s[0], s[1], s[2]])
                    .collect(),
            )
            .user_data(69420)
            .build(),
        ))
        .with_component(cyl_trans)
        .with_component(TagComponent::new("upside-ground"));

    let bouncing_ball = AddEntity::new(&mut *sbsts.world.borrow_mut(), None)
        .unwrap()
        .with_component(RigidBodyComponent::from_rigid_body(
            RigidBodyBuilder::new(RigidBodyType::Dynamic)
                .translation(vector![10.0, 10.0, 0.0])
                .build(),
        ))
        .with_component(ColliderComponent::from_collider(
            ColliderBuilder::ball(5.0).restitution(0.7).build(),
        ))
        .with_component(TransformComponent::identity().scale(cgmath::vec3(50.0, 50.0, 50.0)))
        .with_component(TagComponent::new("Bouncing ball"))
        .entity;

    crate::utils::load_gltf_into_world(
        "ball.glb",
        &mut *sbsts.world.borrow_mut(),
        bouncing_ball,
        sbsts.get_vulkan_common().unwrap(),
        &sbsts.physfs,
    );

    crate::utils::load_gltf_into_world(
        "ground.glb",
        &mut *sbsts.world.borrow_mut(),
        World::get_root(),
        sbsts.get_vulkan_common().unwrap(),
        &sbsts.physfs,
    );
}
