use std::collections::{HashMap, HashSet, VecDeque};
use std::convert::TryFrom;
use std::hash::BuildHasher;
use std::ops::DerefMut;
use std::sync::Arc;

use game_kernel::game_kernel::core_systems::static_mesh::*;
use game_kernel::game_kernel::core_systems::{
    relative_transform::RelativeTransformComponent, tag::TagComponent,
};
use game_kernel::game_kernel::ecs::*;
use game_kernel::physfs_rs::PhysFs;
use game_kernel::renderer::{material::*, mesh::*, model::*, texture::*};
use game_kernel::vulkan::VulkanBasic;
use game_kernel::TransformComponent;

type KeyType = u64;

use cgmath::*;
use gltf::animation::Target;

pub trait CloneComponent: Component {
    fn get_component(&self) -> Box<dyn Component>;
}

impl<T: Component + Clone> CloneComponent for T {
    fn get_component(&self) -> Box<dyn Component> {
        Box::new(self.clone())
    }
}

pub struct ImportedSceneNode {
    children: Vec<usize>,
    components: Vec<Box<dyn CloneComponent>>,
}

impl ImportedSceneNode {
    pub fn new() -> Self {
        Self {
            children: vec![],
            components: vec![],
        }
    }

    pub fn add_child(&mut self, child: usize) {
        self.children.push(child);
    }

    pub fn add_component<C: Component + Clone>(&mut self, c: C) {
        self.components.push(Box::new(c));
    }

    pub fn with_component<C: Component + Clone>(self, c: C) -> Self {
        let mut s = self;
        s.add_component(c);

        s
    }

    pub fn get_children(&self) -> impl Iterator<Item = usize> + '_ {
        self.children.iter().cloned()
    }

    pub fn get_component(&self) -> impl Iterator<Item = Box<dyn Component>> + '_ {
        self.components.iter().map(|c| c.get_component())
    }

    pub fn add_to_world(&self, world: &mut World, parent: KeyType) -> KeyType {
        let entity = world.add_entity(parent).unwrap();
        for component in self.get_component() {
            world.add_component(entity, component);
        }

        entity
    }
}

pub struct ImportedScene {
    v: Vec<ImportedSceneNode>,
    //gltf(or whatever) index -> index into v
    pub index_map: Option<HashMap<usize, usize>>,
}

impl ImportedScene {
    pub fn new() -> Self {
        Self {
            v: vec![ImportedSceneNode::new()],
            index_map: None,
        }
    }

    pub fn add_node(&mut self, node: ImportedSceneNode) -> usize {
        self.v.push(node);

        self.v.len() - 1
    }

    pub fn get_node(&self, index: usize) -> Option<&ImportedSceneNode> {
        self.v.get(index)
    }

    pub fn get_node_mut(&mut self, index: usize) -> Option<&mut ImportedSceneNode> {
        self.v.get_mut(index)
    }

    pub fn load_into_world(&self, world: &mut World, wroot: KeyType) -> HashMap<usize, KeyType> {
        let mut queue = VecDeque::new();
        queue.push_back(0);

        let mut original_world_index_map = HashMap::new();
        original_world_index_map.insert(0, wroot);

        while let Some(parent) = queue.pop_front() {
            for child in self.v[parent].get_children() {
                let entity = self
                    .get_node(child)
                    .unwrap()
                    .add_to_world(world, original_world_index_map[&parent]);
                original_world_index_map.insert(child, entity);
                queue.push_back(child);
            }
        }

        original_world_index_map
    }
}

fn add_node_to_world<'a>(
    scene: &mut ImportedScene,
    parent: usize,
    node: &gltf::Node<'a>,
    vk: impl VulkanBasic,
    fs: &PhysFs,
    buffer: &gltf::buffer::Data,
) -> usize {
    let mut builder = ImportedSceneNode::new().with_component({
        let (translation, rotation, scale) = node.transform().decomposed();

        let rotation = {
            let [x, y, z, s] = rotation;
            [s, x, y, z]
        };

        RelativeTransformComponent::identity()
            .translate(translation.into())
            .rotate(rotation)
            .scale(scale.into())
    });

    if let Some(mesh) = node.mesh() {
        builder.add_component(mesh_component_from_node(vk, fs, mesh, buffer))
    }

    if let Some(name) = node.name() {
        builder.add_component(TagComponent::new(name.to_owned()));
    }

    let new_node = scene.add_node(builder);
    scene.get_node_mut(parent).unwrap().add_child(new_node);
    new_node
}

pub fn load_gltf(path: &str, vk: impl VulkanBasic, fs: &PhysFs) -> ImportedScene {
    let (document, buffers, images) =
        gltf::import_slice(fs.open_read(path).unwrap().read_to_vec().unwrap()).unwrap();
    let mut nodes = document.default_scene().unwrap().nodes();

    let mut queue = VecDeque::new();

    let mut index_map = HashMap::new();

    let mut imported_scene = ImportedScene::new();

    for root in nodes {
        let wk = add_node_to_world(&mut imported_scene, 0, &root, vk.clone(), fs, &buffers[0]);
        index_map.insert(root.index(), wk);
        queue.push_back(root);
    }

    while let Some(parent) = queue.pop_front() {
        for child in parent.children() {
            let wk = add_node_to_world(
                &mut imported_scene,
                *index_map.get(&parent.index()).unwrap(),
                &child,
                vk.clone(),
                fs,
                &buffers[0],
            );
            index_map.insert(child.index(), wk);
            queue.push_back(child);
        }
    }

    imported_scene.index_map = Some(index_map);
    imported_scene
}

pub fn load_gltf_into_world(
    path: &str,
    world: &mut World,
    wroot: KeyType,
    vk: impl VulkanBasic,
    fs: &PhysFs,
) -> HashMap<usize, KeyType> {
    load_gltf(path, vk, fs).load_into_world(world, wroot)
}

fn test_material(vk: impl VulkanBasic) -> Material {
    let tdata = TextureData::from_uniform_color(Vector3::new(0.8, 0.8, 0.8));
    let test_texture =
        Texture::from_texture_data(&tdata, TextureUsage::Color, vk.clone(), Some(RGB8LINEAR))
            .unwrap();
    let tdata = TextureData::from_uniform_color(Vector3::new(0.5, 0.5, 1.0));
    let test_ntexture =
        Texture::from_texture_data(&tdata, TextureUsage::Normal, vk.clone(), Some(RGB8LINEAR))
            .unwrap();
    let tdata = TextureData::from_uniform_color(Vector3::new(0.2, 0.2, 0.2));
    let test_rtexture = Texture::from_texture_data(
        &tdata,
        TextureUsage::Roughness,
        vk.clone(),
        Some(RGB8LINEAR),
    )
    .unwrap();
    let tdata = TextureData::from_uniform_color(Vector3::new(0.1, 0.1, 0.1));
    let test_mtexture = Texture::from_texture_data(
        &tdata,
        TextureUsage::Metalness,
        vk.clone(),
        Some(RGB8LINEAR),
    )
    .unwrap();

    let tdata = TextureData::from_uniform_color(Vector3::new(0.0, 0.0, 0.0));
    let test_etexture =
        Texture::from_texture_data(&tdata, TextureUsage::Emission, vk.clone(), Some(RGB8LINEAR))
            .unwrap();

    unsafe {
        Material::test_new(
            vk.get_device(),
            Arc::new(test_texture),
            Arc::new(test_ntexture),
            Arc::new(test_rtexture),
            Arc::new(test_mtexture),
            Arc::new(test_etexture),
        )
    }
}

use gltf::scene::Transform;
use image::io::Reader;
use image::{DynamicImage, GenericImageView, ImageFormat};

fn image_texture_data(
    fs: &PhysFs,
    buffer: &gltf::buffer::Data,
    source: gltf::image::Source,
    swizzle: [u8; 4],
) -> TextureData {
    let decoded = match source {
        gltf::image::Source::Uri { uri, .. } => {
            let read = fs.open_read(uri).unwrap();
            Reader::new(BufReader::new(read))
                .with_guessed_format()
                .unwrap()
                .decode()
                .unwrap()
        }
        gltf::image::Source::View { view, .. } => {
            let offset = view.offset();
            let stride = view.stride().unwrap_or(1);
            let length = view.length();

            let d = &buffer[offset..offset + length];
            Reader::new(std::io::Cursor::new(d))
                .with_guessed_format()
                .unwrap()
                .decode()
                .unwrap()
        }
    };

    match decoded {
        DynamicImage::ImageRgb8(image) => TextureData {
            gl_internal_format: 35907, //TODO dont waste the alpha channel
            size: [image.width(), image.height()],
            is_cubemap: false,
            mip_levels: 1,
            layers_data: vec![image
                .pixels()
                .flat_map(|p| {
                    [
                        p.0[swizzle[0] as usize],
                        p.0[swizzle[1] as usize],
                        p.0[swizzle[2] as usize],
                        255,
                    ]
                })
                .collect()],
        },
        DynamicImage::ImageRgba8(image) => TextureData {
            gl_internal_format: 35907,
            size: [image.width(), image.height()],
            is_cubemap: false,
            mip_levels: 1,
            layers_data: vec![image
                .pixels()
                .flat_map(|p| {
                    [
                        p.0[swizzle[0] as usize],
                        p.0[swizzle[1] as usize],
                        p.0[swizzle[2] as usize],
                        p.0[swizzle[2] as usize],
                    ]
                })
                .collect()],
        },
        //todo warning
        _ => TextureData::from_uniform_color(vec3(1.0, 1.0, 1.0)),
    }
}

const SWIZZLE_IDENTITY: [u8; 4] = [0, 1, 2, 3];

fn gltf_material(
    vk: impl VulkanBasic,
    fs: &PhysFs,
    buffer: &gltf::buffer::Data,
    material: &gltf::Material,
) -> Material {
    let (tdata, color_space) =
        if let Some(base_color_texture) = material.pbr_metallic_roughness().base_color_texture() {
            (
                image_texture_data(
                    fs,
                    buffer,
                    base_color_texture.texture().source().source(),
                    SWIZZLE_IDENTITY,
                ),
                SRGB,
            )
        } else {
            let color = material.pbr_metallic_roughness().base_color_factor();
            (
                TextureData::from_uniform_color(Vector3::new(color[0], color[1], color[2])),
                RGB8LINEAR,
            )
        };
    let test_texture =
        Texture::from_texture_data(&tdata, TextureUsage::Color, vk.clone(), Some(color_space))
            .unwrap();

    let tdata = if let Some(base_color_texture) = material.normal_texture() {
        image_texture_data(
            fs,
            buffer,
            base_color_texture.texture().source().source(),
            SWIZZLE_IDENTITY,
        )
    } else {
        TextureData::from_uniform_color(Vector3::new(0.5, 0.5, 1.0))
    };
    let test_ntexture =
        Texture::from_texture_data(&tdata, TextureUsage::Normal, vk.clone(), Some(RGB8LINEAR))
            .unwrap();

    let tdata = if let Some(base_color_texture) = material
        .pbr_metallic_roughness()
        .metallic_roughness_texture()
    {
        image_texture_data(
            fs,
            buffer,
            base_color_texture.texture().source().source(),
            [2, 1, 0, 0],
        )
    } else {
        let p = material.pbr_metallic_roughness();
        TextureData::from_uniform_color(Vector3::new(
            p.metallic_factor(),
            p.roughness_factor(),
            1.0,
        ))
    };
    let test_mrtexture = Texture::from_texture_data(
        &tdata,
        TextureUsage::MetalnessRoughness,
        vk.clone(),
        Some(RGB8LINEAR),
    )
    .unwrap();

    let emissive_tdata = if let Some(emissive_texture) = material.emissive_texture() {
        image_texture_data(
            fs,
            buffer,
            emissive_texture.texture().source().source(),
            SWIZZLE_IDENTITY,
        )
    } else {
        let p = material.emissive_factor();
        TextureData::from_uniform_color(Vector3::new(p[0], p[1], p[2]))
    };

    let emissive_texture = Texture::from_texture_data(
        &emissive_tdata,
        TextureUsage::Emission,
        vk.clone(),
        Some(RGB8LINEAR),
    )
    .unwrap();

    unsafe {
        Material::test_new_combined(
            vk.get_device(),
            Arc::new(test_texture),
            Arc::new(test_ntexture),
            Arc::new(test_mrtexture),
            Arc::new(emissive_texture),
        )
    }
}

fn mesh_component_from_node<'a>(
    vk: impl VulkanBasic,
    fs: &PhysFs,
    node: gltf::Mesh<'a>,
    buffer: &gltf::buffer::Data,
) -> StaticMeshComponent {
    let queue = vk.get_graphics_queue();
    let primitive = node.primitives().next().unwrap();
    let material = primitive.material();
    let cube_mesh = Mesh::StaticMesh(StaticMesh::from_mesh_data(
        queue,
        &MeshData::try_from((primitive, buffer)).unwrap(),
    ));

    let material = gltf_material(vk, fs, buffer, &material); //test_material(vk);

    StaticMeshComponent::new(Arc::new(material), Arc::new(cube_mesh))
}

#[derive(Clone, Debug)]
pub enum KeyFrameVal {
    Translation(cgmath::Vector3<f32>),
    Rotation(cgmath::Quaternion<f32>),
    Scale(cgmath::Vector4<f32>),
}

#[derive(Clone, Debug)]
struct KeyFrame {
    time: f32,
    key_vals: Vec<(KeyFrameVal, KeyType)>,
}

#[derive(Clone, Debug)]
pub struct TransformHierarchyNode {
    pub transform: RelativeTransformComponent,
    pub children: Vec<KeyType>,
}

#[derive(Clone)]
pub struct TransformHierarchy {
    root: KeyType,
    nodes: HashMap<KeyType, TransformHierarchyNode>,
}

impl TransformHierarchy {
    pub fn from_entity(world: &World, root: KeyType, filter: Option<HashSet<KeyType>>) -> Self {
        let mut nodes = HashMap::new();
        let mut q = VecDeque::new();

        q.push_back(root);

        let root_transform = world
            .get_entity_components::<&RelativeTransformComponent>(root)
            .as_deref()
            .cloned()
            .unwrap_or(RelativeTransformComponent::identity());

        let root_node = TransformHierarchyNode {
            transform: root_transform,
            children: vec![],
        };

        nodes.insert(root, root_node);

        while let Some(parent) = q.pop_front() {
            for child in world.get_children(parent).unwrap() {
                q.push_back(child);
            }

            for child in world.get_children(parent).unwrap() {
                if filter.is_some() && !filter.as_ref().unwrap().contains(&child) {
                    continue;
                }
                if let Some(child_relative) =
                    world.get_entity_components::<&RelativeTransformComponent>(child)
                {
                    nodes.insert(
                        child,
                        TransformHierarchyNode {
                            transform: child_relative.clone(),
                            children: vec![],
                        },
                    );
                }
                nodes.get_mut(&parent).unwrap().children.push(child);
            }
        }

        Self { root, nodes }
    }

    pub fn interpolate(&self, other: &Self, factor: f32) -> Self {
        let mut res = self.clone();

        for ((_, ta), (_, tb)) in res.nodes.iter_mut().zip(other.nodes.iter()) {
            ta.transform = ta.transform.interpolate(&tb.transform, factor);
        }

        res
    }
}

pub struct Animation {
    key_frames: Vec<(KeyFrame, Option<Vec<(KeyType, RelativeTransformComponent)>>)>,
}

impl Animation {
    fn cache_hierarchies(&mut self, start_frame: TransformHierarchy) {
        //let mut current_hierarchy = start_frame;
        let mut ent_set = HashMap::new();

        for (key_frame, cached) in self.key_frames.iter_mut() {
            for (key_val, target) in key_frame.key_vals.iter() {
                //current_hierarchy[target]
                if !ent_set.contains_key(target) {
                    ent_set.insert(*target, RelativeTransformComponent::identity());
                }
                match key_val {
                    KeyFrameVal::Translation(t) => {
                        ent_set.get_mut(target).as_mut().unwrap().position = *t
                    }
                    KeyFrameVal::Rotation(r) => {
                        ent_set.get_mut(target).as_mut().unwrap().rotation = *r
                    }
                    KeyFrameVal::Scale(s) => {
                        ent_set.get_mut(target).as_mut().unwrap().scale = s.truncate()
                    }
                }
            }

            *cached = Some(ent_set.iter().map(|(a, b)| (*a, b.clone())).collect());
        }
    }

    fn interpolate<'a>(
        a: &'a Vec<(KeyType, RelativeTransformComponent)>,
        b: &'a Vec<(KeyType, RelativeTransformComponent)>,
        fac: f32,
    ) -> impl Iterator<Item = (KeyType, RelativeTransformComponent)> + 'a {
        a.iter()
            .map(|(i, e)| (e, b.iter().find(|(bi, _)| bi == i).unwrap()))
            .map(move |(a, (i, b))| (*i, a.interpolate(b, fac)))
    }

    pub fn frame_transform(&self, time: f32) -> Vec<(KeyType, RelativeTransformComponent)> {
        std::iter::once(&self.key_frames[0])
            .chain(self.key_frames.iter())
            .zip(self.key_frames.iter())
            .find(|(_, (s, _))| s.time >= time)
            .map(|((kfa, ca), (kfb, cb))| {
                let factor = (kfb.time - time) / (kfb.time - kfa.time);

                Self::interpolate(cb.as_ref().unwrap(), ca.as_ref().unwrap(), factor).collect()
            })
            .unwrap_or_else(|| {
                self.key_frames
                    .first()
                    .unwrap()
                    .1
                    .as_ref()
                    .unwrap()
                    .iter()
                    .map(|(a, b)| (*a, b.clone()))
                    .collect()
            })
    }

    pub fn play(&self, world: &mut World, time: f32) {
        self.frame_transform(time)
            .into_iter()
            .for_each(|(entity, transform)| {
                let comp = world.get_entity_components::<&mut RelativeTransformComponent>(entity);

                if let Some(mut comp) = comp {
                    *comp = transform.clone();
                }
            });
    }
}

//TODO use the one from the engine
use std::cmp::{PartialEq, PartialOrd};

#[derive(PartialOrd, Debug)]
struct OrdF32(f32);

impl PartialEq for OrdF32 {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}

impl Eq for OrdF32 {}

fn ord_f32(v: f32) -> OrdF32 {
    OrdF32(v)
}

impl core::cmp::Ord for OrdF32 {
    fn cmp(&self, other: &Self) -> core::cmp::Ordering {
        if self.0 < other.0 {
            core::cmp::Ordering::Less
        } else if self.0 > other.0 {
            core::cmp::Ordering::Greater
        } else {
            core::cmp::Ordering::Equal
        }
    }

    fn max(self, other: Self) -> Self {
        ord_f32(self.0.max(other.0))
    }

    fn min(self, other: Self) -> Self {
        ord_f32(self.0.min(other.0))
    }

    /*fn clamp(self, min: Self, max: Self) -> Self {
        self.v.clamp(min, max))
    }*/
}

fn property_stride(property: gltf::animation::Property) -> usize {
    match property {
        gltf::animation::Property::Translation => 12,
        gltf::animation::Property::Rotation => 16,
        gltf::animation::Property::Scale => 12,
        gltf::animation::Property::MorphTargetWeights => 4,
    }
}
//-------------------------------------------------------------------

use byteorder::{ByteOrder, LittleEndian};
use std::collections::BTreeMap;

impl<'a>
    TryFrom<(
        gltf::animation::Animation<'a>,
        &TransformHierarchy,
        &gltf::buffer::Data,
        &HashMap<usize, KeyType>,
    )> for Animation
{
    type Error = &'static str;

    fn try_from(
        (gltf_anim, first_keyframe, data, node_indices_map): (
            gltf::animation::Animation<'a>,
            &TransformHierarchy,
            &gltf::buffer::Data,
            &HashMap<usize, KeyType>,
        ),
    ) -> Result<Self, Self::Error> {
        let mut keyframes_map: BTreeMap<_, Vec<_>> = BTreeMap::new();
        for (target, sampler) in gltf_anim
            .channels()
            .map(|channel| (channel.target(), channel.sampler()))
        {
            //times
            let times_offset = sampler.input().view().unwrap().offset();
            let times_stride = sampler.input().view().unwrap().stride().unwrap_or(4);
            let times_length = sampler.input().view().unwrap().length();

            let times_iterator = data[times_offset..times_offset + times_length]
                .chunks_exact(times_stride)
                .map(|time| LittleEndian::read_f32(time));

            let values_offset = sampler.output().view().unwrap().offset();
            let values_stride = sampler
                .output()
                .view()
                .unwrap()
                .stride()
                .unwrap_or(property_stride(target.property()));
            let values_length = sampler.output().view().unwrap().length();

            let values_iterator =
                data[values_offset..values_offset + values_length].chunks_exact(values_stride);

            match target.property() {
                gltf::animation::Property::Translation => {
                    let translations_iterator = values_iterator.map(|value| {
                        Vector3::new(
                            LittleEndian::read_f32(value),
                            LittleEndian::read_f32(&value[4..]),
                            LittleEndian::read_f32(&value[8..]),
                        )
                    });

                    for (time, translation) in times_iterator.zip(translations_iterator) {
                        if !node_indices_map.contains_key(&target.node().index()) {
                            /*log_warn!(
                                "target \"{}\" ( index {}) not foundi in skeleton",
                                &target.node().name().unwrap_or(""),
                                target.node().index()
                            );*/
                            continue;
                        }

                        if let Some(kf) = keyframes_map.get_mut(&ord_f32(time)) {
                            kf.push((
                                KeyFrameVal::Translation(translation),
                                node_indices_map[&target.node().index()],
                            ));
                        } else {
                            keyframes_map.insert(
                                ord_f32(time),
                                vec![(
                                    KeyFrameVal::Translation(translation),
                                    node_indices_map[&target.node().index()],
                                )],
                            );
                        }
                    }
                }

                gltf::animation::Property::Rotation => {
                    let rotations_iterator = values_iterator.map(|value| {
                        Quaternion::new(
                            LittleEndian::read_f32(&value[12..]),
                            LittleEndian::read_f32(value),
                            -LittleEndian::read_f32(&value[4..]),
                            -LittleEndian::read_f32(&value[8..]),
                        )
                    });

                    for (time, rotation) in times_iterator.zip(rotations_iterator) {
                        if !node_indices_map.contains_key(&target.node().index()) {
                            /*log_warn!(
                                "target \"{}\" ( index {}) not foundi in skeleton",
                                &target.node().name().unwrap_or(""),
                                target.node().index()
                            );*/
                            continue;
                        }

                        if let Some(kf) = keyframes_map.get_mut(&ord_f32(time)) {
                            kf.push((
                                KeyFrameVal::Rotation(rotation),
                                node_indices_map[&target.node().index()],
                            ));
                        } else {
                            keyframes_map.insert(
                                ord_f32(time),
                                vec![(
                                    KeyFrameVal::Rotation(rotation),
                                    node_indices_map[&target.node().index()],
                                )],
                            );
                        }
                    }
                }

                gltf::animation::Property::Scale => {
                    let scales_iterator = values_iterator.map(|value| {
                        Vector3::new(
                            LittleEndian::read_f32(value),
                            LittleEndian::read_f32(&value[4..]),
                            LittleEndian::read_f32(&value[8..]),
                        )
                    });

                    for (time, scale) in times_iterator.zip(scales_iterator) {
                        if !node_indices_map.contains_key(&target.node().index()) {
                            /*log_warn!(
                                "target \"{}\" ( index {}) not foundi in skeleton",
                                &target.node().name().unwrap_or(""),
                                target.node().index()
                            );*/
                            continue;
                        }

                        if let Some(kf) = keyframes_map.get_mut(&ord_f32(time)) {
                            kf.push((
                                KeyFrameVal::Scale(scale.extend(1f32)),
                                node_indices_map[&target.node().index()],
                            ));
                        } else {
                            keyframes_map.insert(
                                ord_f32(time),
                                vec![(
                                    KeyFrameVal::Scale(scale.extend(1f32)),
                                    node_indices_map[&target.node().index()],
                                )],
                            );
                        }
                    }
                }

                _ => {}
            }

            //for keyframe in
        }

        let mut anim = Self {
            key_frames: keyframes_map
                .iter()
                .map(|(time, kf)| {
                    (
                        KeyFrame {
                            time: time.0,
                            key_vals: kf.clone(),
                        },
                        None,
                    )
                })
                .collect(),
        };
        anim.cache_hierarchies(first_keyframe.clone());
        Ok(anim)
    }
}

pub fn load_test_cube(vk: impl VulkanBasic) -> StaticMeshComponent {
    let (document, buffers, images) = gltf::import("files/cube.glb").unwrap();
    let mut nodes = document.default_scene().unwrap().nodes();

    let cube_node = nodes.next().unwrap();

    let queue = vk.get_graphics_queue();
    let cube_mesh = Mesh::StaticMesh(StaticMesh::from_mesh_data(
        queue,
        &MeshData::try_from((
            cube_node.mesh().unwrap().primitives().next().unwrap(),
            &buffers[0],
        ))
        .unwrap(),
    ));

    let tdata = TextureData::from_uniform_color(Vector3::new(0.8, 0.8, 0.8));
    let test_texture =
        Texture::from_texture_data(&tdata, TextureUsage::Color, vk.clone(), Some(RGB8LINEAR))
            .unwrap();
    let tdata = TextureData::from_uniform_color(Vector3::new(0.5, 0.5, 1.0));
    let test_ntexture =
        Texture::from_texture_data(&tdata, TextureUsage::Normal, vk.clone(), Some(RGB8LINEAR))
            .unwrap();
    let tdata = TextureData::from_uniform_color(Vector3::new(0.2, 0.2, 0.2));
    let test_rtexture = Texture::from_texture_data(
        &tdata,
        TextureUsage::Roughness,
        vk.clone(),
        Some(RGB8LINEAR),
    )
    .unwrap();
    let tdata = TextureData::from_uniform_color(Vector3::new(0.2, 0.2, 0.2));
    let test_mtexture = Texture::from_texture_data(
        &tdata,
        TextureUsage::Metalness,
        vk.clone(),
        Some(RGB8LINEAR),
    )
    .unwrap();

    let tdata = TextureData::from_uniform_color(Vector3::new(0.0, 0.0, 0.0));
    let test_etexture =
        Texture::from_texture_data(&tdata, TextureUsage::Emission, vk.clone(), Some(RGB8LINEAR))
            .unwrap();

    let material = unsafe {
        Material::test_new(
            vk.get_device(),
            Arc::new(test_texture),
            Arc::new(test_ntexture),
            Arc::new(test_rtexture),
            Arc::new(test_mtexture),
            Arc::new(test_etexture),
        )
    };

    StaticMeshComponent::new(Arc::new(material), Arc::new(cube_mesh))
}

use std::fs::File;
use std::io::{BufReader, Read, Seek};

pub fn load_skeletal_mesh<'a>(
    mesh_node: &gltf::Node<'a>,
    armature_node: &gltf::scene::Node<'a>,
    buffer: &gltf::buffer::Data,
    vk: impl VulkanBasic,
) -> (Mesh, Skeleton) {
    let mut skeletal_data =
        SkeletalMeshData::try_from((mesh_node.clone(), armature_node.clone(), buffer)).unwrap();

    let skeleton = skeletal_data.skeleton.clone();
    skeletal_data.skeleton = skeleton.clone().skeleton_frame();

    let skeletal_mesh =
        SkeletalMesh::from_skeleton_data(vk.get_transfers_queue(), &skeletal_data).unwrap();

    (Mesh::SkeletalMesh(skeletal_mesh), skeleton)
}

pub fn load_skeletal_animations(
    document: gltf::Document,
    buffer: &gltf::buffer::Data,
    skeleton: &Skeleton,
    vk: impl VulkanBasic,
) -> Vec<SkeletalAnimation> {
    document
        .animations()
        .map(|a| SkeletalAnimation::try_from((a, skeleton, buffer)))
        .filter(Result::is_ok)
        .map(Result::unwrap)
        .collect()
}

pub struct ImportedSkeletalAnimation {
    mesh_component: StaticMeshComponent,
    mesh: Mesh,
    animations: Vec<SkeletalAnimation>,
    transform: Transform,
}

impl ImportedSkeletalAnimation {
    pub fn new(path: impl AsRef<str>, vk: impl VulkanBasic, fs: &PhysFs) -> Self {
        let (document, buffers, _images) =
            gltf::import_slice(fs.open_read(path).unwrap().read_to_vec().unwrap()).unwrap();

        let armature_node = document
            .nodes()
            .find(|n| n.name().map(|name| name == "Armature").unwrap_or(false))
            .unwrap();

        let mesh_node = document
            .nodes()
            .find(|n| n.name().map(|name| name == "armMesh").unwrap_or(false))
            .unwrap();

        let (mut mesh, skeleton) =
            load_skeletal_mesh(&mesh_node, &armature_node, &buffers[0], vk.clone());
        if let Mesh::SkeletalMesh(ref mut mesh) = mesh {
            mesh.mesh.cull_mode = CullMode::Back;
        }

        let material = gltf_material(
            vk.clone(),
            fs,
            &buffers[0],
            &mesh_node
                .mesh()
                .unwrap()
                .primitives()
                .next()
                .unwrap()
                .material(),
        ); //test_material(vk.clone());

        let mesh_component = StaticMeshComponent::new(Arc::new(material), Arc::new(mesh.clone()));

        let transform = armature_node.transform();
        let animations = load_skeletal_animations(document, &buffers[0], &skeleton, vk);

        Self {
            mesh_component,
            mesh,
            animations,
            transform,
        }
    }

    pub fn load_into_world(&self, world: &mut World, entity: Option<KeyType>) {
        AddEntity::new(world, entity)
            .unwrap()
            .with_component(self.mesh_component.clone())
            .with_component({
                let (translation, rotation, scale) = self.transform.clone().decomposed();

                let rotation = {
                    let [x, y, z, s] = rotation;
                    [s, x, y, z]
                };

                RelativeTransformComponent::identity()
                    .translate(translation.into())
                    .rotate(rotation)
                    .scale(scale.into())
            });
    }
}

pub fn skeletal_animation_test(
    path: impl AsRef<str>,
    vk: impl VulkanBasic,
    fs: &PhysFs,
    world: &mut World,
    entity: Option<KeyType>,
) -> ImportedSkeletalAnimation {
    let imported = ImportedSkeletalAnimation::new(path, vk, fs);
    imported.load_into_world(world, entity);

    imported
}

pub fn anim_test(
    path: impl AsRef<str>,
    world: &mut World,
    entity: KeyType,
    vk: impl VulkanBasic,
    fs: &PhysFs,
) -> Vec<Animation> {
    let imported_set = load_gltf(path.as_ref(), vk, fs).load_into_world(world, entity);

    let hierarchy = TransformHierarchy::from_entity(
        world,
        entity,
        Some(imported_set.values().cloned().collect()),
    );

    let (document, buffers, images) =
        gltf::import_slice(fs.open_read(path).unwrap().read_to_vec().unwrap()).unwrap();
    document
        .animations()
        .map(|a| Animation::try_from((a, &hierarchy, &buffers[0], &imported_set)).unwrap())
        .collect()
}

pub mod particles {
    use game_kernel::{
        physfs_rs::PhysFs,
        renderer::{particle_fx::*, texture::*},
        vulkan::VulkanBasic,
    };

    use cgmath::{prelude::*, Vector2, Vector3};

    use std::sync::Arc;

    pub fn test_particle_builder(
        fs: &PhysFs,
        vk: impl VulkanBasic,
    ) -> Arc<ParticleSystemBuilderWrapper> {
        let spark_atlas = Texture::from_texture_data(
            &TextureData::load_from_kxt(fs.open_read("sparks.ktx").unwrap()).unwrap(),
            TextureUsage::Color,
            vk.clone(),
            Some(SRGB),
        )
        .unwrap();

        let sparks_pfx = DynamicParticleSystemBuilder::new(
            Box::new(SphereEmitter::new(
                cgmath::Vector3::new(0.0, 0.0, 0.0),
                cgmath::Vector3::new(10.1, 10.1, 10.1),
                0.01,
                5000.0,
            ) /*.limited(Some(1), None)*/)
            .wrapped(),
            Arc::new(Box::new(move |vk, output, depth, bufs| {
                let psr: Box<AlphaParticleRenderer> = Box::new(ParticlesRendererCreation::new(
                    vk,
                    output,
                    depth,
                    bufs,
                    AlphaParticleRendererParams {
                        life: 0.3,
                        size: Vector2::new(0.03, 0.03),
                        texture: ParticleTexture {
                            atlas: spark_atlas.clone(),
                            offset: Vector2::new(0.0, 0.0),
                            size: Vector2::new(1.0, 1.0),
                            fps: Vector2::new(10.0, 10.0 / 3.0),
                            frames: Vector2::new(1, 1),
                        },
                        blending: ParticleBlending::Additive,
                    },
                ));
                psr
            })),
            Arc::new(Box::new(|vk, max| {
                Box::new(SimpleParticlePhysics::new(
                    vk,
                    max,
                    Vector3::new(0.0, -9.8, 0.0),
                ))
            })),
            10000,
        )
        .wrapped();

        Arc::new(sparks_pfx)
    }
}

pub fn load_gltf_mesh(
    path: impl AsRef<str>,
    vk: impl VulkanBasic,
    fs: &PhysFs,
    name: impl AsRef<str>,
) -> Option<(MeshData<u32>, TransformComponent)> {
    let (document, buffers, _images) =
        gltf::import_slice(fs.open_read(path).unwrap().read_to_vec().unwrap()).unwrap();

    let node = document
        .nodes()
        .find(|node| node.name().is_some() && node.name().unwrap() == name.as_ref())?;
    let mesh = node.mesh()?;

    let queue = vk.get_graphics_queue();
    let primitive = mesh.primitives().next()?;

    let (translation, rotation, scale) = node.transform().decomposed();

    let rotation = {
        let [x, y, z, s] = rotation;
        [s, x, y, z]
    };

    let transform = TransformComponent::identity()
        .translate(translation.into())
        .rotate(rotation)
        .scale(scale.into());

    Some((
        MeshData::try_from((primitive, &buffers[0])).unwrap(),
        transform,
    ))
}
