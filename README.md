Game Kernel test game
=======

About 
-----------
This project is a small first person shooter made to test
[GameKernel](https://gitlab.com/pac85/GameKernel) 

Building
-----------

```
git clone https://gitlab.com/pac85/GameKernel
git clone https://gitlab.com/pac85/egui_vulkano_unsafe egui_vulkano
git clone https://gitlab.com/pac85/imgui-vulkano-renderer
git clone https://github.com/pac85/ruwren
git clone https://github.com/pac85/game-kernel-editor
git clone https://gitlab.com/pac85/game-kernel-test-game
cd game-kernel-test-game
cargo run --release --bin=game-kernel-test-game
```

Screenshots
-----------

![editor](screenshots/ed.png)
![game](screenshots/game.png)

Video
-----------

[Short video demostrating the game](https://www.youtube.com/watch?v=mtDVvhRrF-c)

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/mtDVvhRrF-c" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

