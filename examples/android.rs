use game_kernel_test_game::generic_main;
use ndk_glue;
use std::env;

#[cfg_attr(target_os = "android", ndk_glue::main(backtrace = "on"))]
fn main() {
    env::set_current_dir("/data/data/rust.example.android/files").unwrap();
    println!("GameKernel, current dir: {:?}", env::current_dir());
    println!("Waiting Resume event");
    loop {
        let event = ndk_glue::poll_events();
        if let Some(e) = &event {
            println!("Event {:?}", e);
        }
        if let Some(ndk_glue::Event::WindowHasFocus) = event {
            break;
        }
    }
    println!("Starting engine");
    ndk_glue::native_activity();

    generic_main(game_kernel_test_game::MainArgs::Local, 0);
}
